---
title: Musik
date: 2018-08-09 18:05:37 +0000
thumbnail: "/upload/Header_Musik.jpeg"
year: 2016
description: 'Seit 2012 lädt die Musikschule Friedrichshain-Kreuzberg Musikschüler/innen
  aus den Partnerstädten des Bezirks zu einem gemeinsamen Workshop unter dem Namen
  Zelle 12 ein. '
categories:
- Musik

---
![](/upload/Header_Musik.jpeg)

# Musik

### Musikprojekt Zelle 12 mit Musikschüler und Musikschülerinnen der Musikschule von Friedrichshain-Kreuzberg, Kadıköy (Istanbul), Szczecin (Polen) Wiesbaden und Ingelheim.

Seit 2012 lädt die Musikschule Friedrichshain-Kreuzberg und ihre Leiterin, Frau Ina Finger, Musikschüler/innen aus den Partnerstädten des Bezirks zu einem gemeinsamen Workshop unter dem Namen Zelle 12 ein. Hinter diesem Namen verbirgt sich die Adresse des Konzertsaals der Musikschule in der Zellestraße 12. Seit 2016 sind erstmals auch Schülerinnen aus der Musikschule Kadıköy, dabei.

### 50 Jugendliche aus zwei Kontinenten und 3 Ländern

Fast 50 Jugendliche im Alter von 12 bis 23 Jahren kommen für acht Tage zum gemeinsamen Musizieren zusammen. Die Musikschule Friedrichshain-Kreuzberg stellt die Band und die Gesangsolisten/innen. Die Geigen, Bratschen, Celli und Chorsänger/innen kommen aus den Partnerstädten. Kadıköy war in Begleitung der Leiterin der Musikschule mit 7 Geigerinnen und einer Sängerin vertreten.

### Pop-Musik

Die Themen sind jeweils unterschiedlich, bewegen sich aber alle im Pop Bereich. 2016 wurde mit „Dance classic“ die Verbindung von klassischen Musikstücken zu populären Interpretationen gesucht. 2017 hörten wir 10 Songs in 10 Sprachen unter dem Titel „World Talk“. Die Musikstücke wurden extra für diese Projekte im Auftrag der Musikschule Friedrichshain-Kreuzberg arrangiert.

### Jugendlicher Elan und Professionalität

Die Hauptakteure in diesem Workshop sind die jugendlichen Musiker/innen, die mit ihrem Elan und Ihrer Authentizität alle begeistert haben. Der musikalische Leiter Robert Matt und sein Kollege Günther Neubert haben die jungen Musiker*innen zur Höchstleistung gebracht.

Dem Städtepartnerschaftsverein Kadıköy gelang es, die Kontakte zur Musikschule Kadıköy aufzubauen und zu entwickeln, die Einladung der Schüler/innen zu ermöglichen, Gasteltern zu finden und ihren Aufenthalt in Berlin zu begleiten.

### Erst Berlin, dann Stettin und morgen İstanbul?

Da die Konzerte beim Publikum und den Beteiligten sehr gut ankommen, gibt es den Wunsch, die Konzerte auch in den Partnerstädten und Partnerländern zu zeigen. In 2017 wurde erreicht das Konzert „World Talk“ in der Stettiner Philharmonie auf zu führen.

Eine Einladung nach Kadıköy im Mai 2016 wurde leider durch die Anschläge in Istanbul belastet, die zu einer Absage der Reise der Musikschüler*innen führten. Wir hoffen aber, dass der Besuch und Auftritt in Kadıköy nur aufgeschoben ist.

### 2019 70 Jahre Jubiläum

2019 feiert die Musikschule Friedrichshain-Kreuzberg ihr 70jähriges Jubiläum. Und wir können auf ein besonderes „Zelle 12“ Projekt gespannt sein.

<div class="box">

### Die Musikschule in Kadıköy

Kadıköy Belediyesi Çocuk Sanat Merkezi   
Kinder Kunst ( Musik,Tanz,Theater) Zentrum des Bezirksamtes Kadıköy  
Koordinatorin:Frau F.Yeşim Altınay

[http://www.cocuksanatmerkezi.kadikoy.bel.tr](http://www.cocuksanatmerkezi.kadikoy.bel.tr "http://www.cocuksanatmerkezi.kadikoy.bel.tr")

**Adres :** Fahrettin Kerim Gökay Cad. No:128 Göztepe-Kadıköy  
**Telefon:** +90 216 567 84 01 (pbx), +90 216 565 71 45  
**Fax:** +90 216 565 22 68  
**E-mail:** [cocuk.sanatmerkezi@kadikoy.bel.tr](mailto:cocuk.sanatmerkezi@kadikoy.bel.tr)

### Musikschule Friedrichshain-Kreuzberg

Leiterin: Frau Ina Finger 

[https://www.ms-fk.de]()

**Adres :** Mariannenplatz 2,10997 Berlin  
**Telefon:** +49 90298-1431   
**Fax:** +49 90298-1435  
**E-mail:** [Musikschule@ba-fk.berlin.de]()

</div>