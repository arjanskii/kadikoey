---
title: Begegnungs- und Bildungsreisen
date: 2018-08-12 09:22:41 +0000
thumbnail: "/upload/Header_Bildungsreisen.jpg"
description: Wie werden kommunale Aufgaben in den Bereichen Familie, Kinder, Bildung,
  Soziales, Kultur und Stadtentwicklung in der jeweiligen Partnerstadt umgesetzt?
categories:
- Bildungsreisen
year: 2010

---
![](/upload/Header_Bildungsreisen.jpg)

# Begegnungs- und Bildungsreisen

Die Partnerstadt von innen her kennen lernen, wissen und verstehen wollen wie kommunale Aufgaben in den Bereichen Familie, Kinder, Bildung, Soziales, Kultur und Stadtentwicklung in der jeweiligen Partnerstadt umgesetzt werden, ist seit dem Beginn unsere Städtepartnerschaftsarbeit von großem Interesse.

Durch Bildungsreisen und durch fachlichen Austausch konnten Bürger und Bürgerinnen beider Partnerbezirke den jeweils anderen Bezirk kennen lernen. Interessiert waren insbesondere Mitarbeiter*innen von Projekten und Einrichtungen im Bereich Bildung, Soziales, Stadtentwicklung, Integration behinderter Menschen und Kindertagesstätten.

<div class="box">

### Kooperationspartner

Wir hatten das Glück in den ersten Jahren der partnerschaftlichen Beziehungen mit der stellvertretenen Bürgermeister Inci Beşpınar eine sehr engagierte Ansprechpartnerin zu finden, die uns die Tür zu vielen Bildungs- und Sozialprojekten öffnete. Die Volkshochschule Friedrichshain-Kreuzberg war bei einigen Reisen Kooperationspartner. Dadurch waren die Reisen als Bildungsreise anerkannt.

### Besuch von Projekten in Kadıköy

Familienberatungszentren, _Aile Danışma Merkezleri_  
Frauenkooperativen,  
Frauenhaus Kadıköy,  
Beratungs- und Eingliederungsstellen für Menschen mit Behinderung,  
Zentrum für Menschen mit Behinderung,  
Hilfe Station für Straßenkinder,  
Verein für zeitgenössische Lebensweise, _Cağdaş Yaşam Derneği_  
Verein der ehrenamtlichen von Kadıköy _(KASDAV heute_ _Kadıköy Gönüllüleri_  
Institut für Europäische Studien an der Marmara Universität in Kadıköy  
Kinderbibliothek  
Kindertagesstätten, _Çocuk Yuvaları_  
Erziehungsberatungsstelle ,_Kadıköy Belediyesi ÇocukKoruyucu Ruh Sağlığı Merkezi_  
Gesundheitszentren, _Sağlık Merkezleri_  
Metin Sabancı Bildungs- und Rehabilitationszentrum für Spastische Kinder und Jugendliche /_Metin Sabancı Spastik Çocuklar ve Gençler Eğitim Üretim Rehabilitasyon Merkezi_  
AREM Therapie und Rehabilitationszentrum /_AREM  Avrupa  Rehabilitasyon  Merkezi_  
Grundschulen, _Ilk Okullar  
_Bezirksamt Kadıköy_, Kadıköy Belediyesi/  
_Stadtkommitee, _Kadıköy Kent Konseyi_  
Ortsbeirat von Fikirtepe,  _Fikirtepe muhtarlığı_  
Architektenkammer Istanbul/ _Mimarlar Odasi_  
Goethe Institut Istanbul- Deutschkurse  
Marmara Universität, Abteilung Deutsch für Ausländer und Germanistische Fakultät  
Volkshochschule Kadıköy/ _Kadıköy Halk Eğitim Merkezi_  
Bariş Manco Kulturzentrum  
Cadde Bostan Kulturzentrum

### Besuch von Projekten in Friedrichshain-Kreuzberg

Volkhochschule Friedrichshain-Kreuzberg  
Musikschule Friedrichshain-Kreuzberg  
Bezirksmuseum  
Nachbarschaftszentrum Kotti e.V  
Nachbarschaftshaus Urbanstr. e.V  
Begegnungszentrum Adalbertstraße  
Mehrgenerationenhaus Wassertorstraße  
Frauenzufluchtswohnungen  
Frauenberatungseinrichtungen  
Häusliche Gewalt der Berliner Polizei  
Werkstätten für Menschen mit Behinderung (Integral e.V. und Mosaik e.V.)  
Aziz Nesin Grundschule  
Charlotte Salomon Grundschule  
Kinder- und Jungendambulanz/ SPZ  
Bezirksamt, BVV, Gesundheitsamt, Jugendamt, Sozialamt  
Stadtteilausschuss Kreuzberg  
Kindertagesstätten des VAK e.V. und Villa Waldemar e.V.

uvm.

</div>