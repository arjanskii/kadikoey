---
title: Fotografie
date: 2018-08-01 00:00:00 +0000
thumbnail: "/upload/Header_Fotografie.jpg"
year: 2018
categories:
- Fotografie
- Ausstellung
- Istanbul
- Berlin
description: Gemeinsame Fotoprojekte zwischen Kadıköy und Friedrichshain-Kreuzberg

---
![](/upload/Header_Fotografie.jpg)

# Fotografie

Seit 2012 führt der Städtepartnerschaftsverein Kadıköy e.V.  in Kooperation mit der Gilberto-Bosques-Volkshochschule Friedrichshain-Kreuzberg (VHS)  und dem Nazim Hikmet Kulturzentrum (NHK) in Kadıköy gemeinsame Fotoprojekte durch. Thema war zu Beginn die fotografische Auseinandersetzung mit den Entwicklungen von Stadträumen in Istanbul, die stark von Binnenmigration und rasanten städtebaulichen Veränderungen geprägt sind. Fotografen aus Berlin und Istanbul gingen unter Leitung der Dozenten Klaus W. Eisenlohr und Levent Karaoğlu gemeinsam auf Exkursion, diskutierten die ausgewählte Bilder und stellten eine gemeinsame Präsentation zusammen.

### "Exploring Istanbul"...

Fotografien aus den bisher erfolgreich realisierten Projekten „Exploring Istanbul“ (2013) „Exploring Berlin“(2014) und „UrbanIST“(2016) wurden in Ausstellungen in Istanbul/Kadıköy und Berlin/ Friedrichshain-Kreuzberg gezeigt. Zu jedem Projekt wurde zusätzlich ein Katalog publiziert und in Berlin und Istanbul veröffentlicht. _A_lle Ausstellungen wurden sehr gut besucht und erreichten Anerkennung auch in der Fachöffentlichkeit. Für die Teilnehmer*innen war es ein besonderes Erlebnis, in Berlin und Istanbul auszustellen. Die durch diese Projektarbeit auch auf persönlicher Ebene entstanden Kontakte dauern an.

[PDF-Datei zur Ausstellung](https://app.forestry.io/sites/lxsfifezxvi0ya/body-media//upload/urbanistanbul-ausstellungen_web.pdf "urbanistanbul-ausstellungen_web.pdf")

### Berlin-Istanbul Tandem- Fotoprojekt

Für 2018 planen wir ein weiteres Projekt, das stärker als die bisherigen Projekte durch die Arbeit in gemischten Kleingruppen die Zusammenarbeit und den Austausch zwischen den Berliner und Istanbuler Teilnehmer*innen im Focus hat. Eine Fotogruppe des Nazim Hikmet Kulturzentrums unter Leitung von Levent Karaoğlu wird nach Berlin kommen und zusammen mit dem Kursleiter der VHS, Klaus W. Eisenlohr, und Fotografen aus Berlin arbeiten. Ziel ist es, sich in einem fotografischen Tandem mit Berlin, der Stadt, den Menschen, den besonderen Orten und Gegebenheiten- auseinanderzusetzten.

<div class="box">

### Ausstellungen in Berlin und Istanbul

#### „Exploring Istanbul"
- Ausstellung in Berlin, Alte Feuerwache Projektraum vom 20.Juli bis 16.August 2013
- Ausstellung in Istanbul/ Kadıköy im November 2013 Barış Manco Kültür Merkezi

#### „Exploring Berlin“
- Ausstellung in Istanbul/ Kadıköy November 2013 im Barış Manco Kültür Merkezi
- Ausstellung in Berlin, Alte Feuerwache Projektraum vom 14 Januar bis 22 Februar 2016

#### urbanISTanbul
- Ausstellung in Berlin, 9.bis 16.06.2016 im Projektraum/ Kunstquartier Bethanien
- Ausstellung in Istanbul/ Kadıköy vom 9. bis 22. Dezember 2016 im Caddebostan Kültür Merkezi
[www.urbanistanbul.de](http://www.urbanistanbul.de/)
[www.exhibit.photocentrum.de/urban-istanbul/](http://www.exhibit.photocentrum.de/urban-istanbul/)


</div>