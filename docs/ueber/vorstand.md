---
title: Vorstand
date: 2
excerpt: Unser Vorstand ist deutsch-türkisch

---
# Vorstand

Christiane Zieger-Ayanoğlu  
Vorstandsvorsitzende

Özcan Ayanoğlu  
Stellvertretender Vorstandsvorsitzender

Peter Held  
Kassenwart

Ümit Bayam  
Schriftführer

Valie Djordjevic  
Beisitzerin

Metin Yilmaz  
Beisitzer