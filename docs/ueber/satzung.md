---
title: Satzung
date: 2018-09-09 16:27:26 +0000
excerpt: Satzung des Städtepartnerschaftsvereins Kadıköy e.V.

---
# Satzung

**Satzung des "Städtepartnerschaftsverein Kadıköy e.V."**

§ 1 Name und Sitz

1\. Der Verein führt den Namen "Städtepartnerschaftsverein Kadiköy e.V.".  
2\. Der Sitz des Vereins ist Berlin  
3\. Der Verein ist in das Vereinsregister des Amtsgerichtes Charlottenburg von Berlin eingetragen.

§ 2 Zweck des Vereins

1\. Der Zweck des Vereines ist die Förderung des Austausches zwischen den Einwohnern und Freunden der Gemeinden Berlin - Friedrichshain-Kreuzberg und Istanbul - Kadiköy insbesondere zu gesellschaftspolitischen und kulturellen Belangen mit dem Ziel der Erhaltung des Friedens und der Völkerverständigung. Hauptziel ist die Förderung der internationalen Gemeinschaft und der Toleranz auf allen Gebieten des Verständigungsgedankens.  
2\. Der Satzungszweck wird insbesondere verwirklicht durch  
 - die Verbreitung von Informationen über Kadiköy bzw. dort über Friedrichshain-Kreuzberg  
 - die Organisation von Begegnungen zwischen Menschen aus Friedrichshain-Kreuzberg und Kadiköy  
 - durch die Organisation von Veranstaltungen für den Austausch über Geschichte, Kultur, Stadtentwicklung und soziale Fragen  
 - Unterstützung der Erdbebenopfer  
3\. Der Verein ist überparteilich und überkonfesssionell.

§ 3 Gemeinnützigkeit

1\. Der Verein verfolgt ausschließlich und unmittelbar diese gemeinnützigen Zwecke im Sinne des Abschnittes "Steuerbegünstigte Zwecke" der Abgabenordnung.  
2\. Der Verein ist selbstlos tätig und verfolgt nicht eigenwirtschaftliche Zwecke.  
3\. Mittel des Vereines dürfen nur für satzungsmäßige Zwecke verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.  
4\. Es darf keine Person durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

§ 4 Mitgliedschaft

1\. Mitglieder des Vereines können natürliche und juristische Personen werden.  
2\. Neben den Mitgliedern gibt es Fördermitglieder, die ausschließlich den Verein in seiner Arbeit gemäß § 2 fördern. Diesen Fördermitgliedern stehen nicht die Rechte aus § 8 zu. Ihre Aufnahme erfolgt durch schriftliche Erklärung.  
3\. Die Mitgliedschaft wird durch schriftliche Erklärung gegenüber dem Vorstand beantragt. Der Eintritt erfolgt über Annahme durch de Vorstand. Die Mitgliederversammlung wird über alle Anträge und aufnahmen informiert.  
4\. Die Mitgliedschaft erlischt durch Austritt, Ausschluss oder Tod.  
5\. Der Austritt ist schriftlich mit einer Frist von vier Wochen zum Ende eines Quartals gegenüber dem Vorstand zu erklären.  
6\. Über den Ausschluss eines Mitgliedes entscheidet die Mitgliederversammlung.  
7\. Ist ein Mitglied mit mindestens einem Jahresbeitrag trotz einmaliger schriftlicher Zahlungsaufforderung im Rückstand, ruhen seine Mitgliedsrechte. Die Mitgliedschaft erlischt, ohne daß es eines besonderen Beschlusses der Mitgliederversammlung bedarf, wenn das Mitglied trotz einmaliger, und von Satz 1 unabhängiger schriftlicher Mahnung mit zwei Jahresbeiträgen im Rückstand ist. Das Mitglied ist auf die Folgen hinzuweisen.

§ 5 Finanzen

Die finanziellen Mittel des Vereines setzen sich aus Mitgliedsbeiträgen, privaten Spenden und aus Zuwendungen der öffentlichen Hand zusammen. Über die Annahme von Spenden entscheidet der Vorstand. Die Mitgliederversammlung wird über alle angebotenen und angenommenen Spenden informiert.

§ 6 Mitgliedsbeiträge

1\. Jedes Mitglied ist beitragspflichtig  
Die Höhe der Beiträge und die Fälligkeit werden durch den Vorstand  festgelegt.

§ 7 Geschäftsjahr

Das Geschäftsjahr ist das Kalenderjahr.

§ 8 Organe

Organe des Vereines sind die Mitgliederversammlung und der Vorstand.

§ 9 Mitgliederversammlung

1\. Die Mitgliederversammlung ist in allen Fragen oberstes Beschlussorgan und umfasst alle Mitglieder des Vereines  
2\. .Jedes Mitglied hat eine Stimme. Vertreter juristischer Personen können nicht gleichzeitig als einfache Mitglieder abstimmen.  
3\. Die ordentliche Mitgliederversammlung ist mindestens einmal im Geschäftsjahr einzuberufen. sie ist schriftlich unter Mitteilung der Tagesordnung mit einer Frist von 14 Tagen vom Vorstand einzuberufen.  
4\. Die Mitgliederversammlung ist beschlussfähig, wenn sie ordnungsgemäß einberufen wurde.  
5\. Außerordentliche Mitgliederversammlungen werden auf Beschluss des Vorstandes oder auf schriftlich begründeten Antrag von mindestens 1/10 der Mitglieder einberufen.  
6\. Beschlüsse der Mitgliederversammlung bedürfen der einfachen Mehrheit, Satzungsänderungen einer Mehrheit von 2/3 der abgegebenen Stimmen. Stimmenthaltungen werden bei der Berechnung der Mehrheit nicht mitgezählt. Der Ausschluss eines Mitgliedes bedarf der Mehrheit von 2/3 der Anwesenden. Jeder Antrag auf Ausschluss eines Mitgliedes ist in der Einladung als Tagesordnungspunkt kenntlich zu machen..  
7\. Die Mitgliederversammlung entscheidet über Ziele, Aufgaben und Strukturen des Vereines. Sie beschließt über die Jahresabrechnung, die Entlastung des Vorstandes und der KassenprüferInnen. Sie beschließt den Ausschluss von Mitgliedern  
8\. .Die Mitgliederversammlung wählt den Vorstand, die BeisitzerInnen sowie zwei KassenprüferInnen.  
9\. Die Mitgliederversammlung hat das Recht, Mitglieder des Vorstandes während der laufenden Amtsperiode abzuwählen. Erforderlich ist eine Mehrheit von 2/3 der Anwesenden.  
10\. Über die Mitgliederversammlung ist ein Protokoll zu führen, das von der/dem VersammlungsleiterIn und der/dem Schriftführer abzuzeichnen ist. Dieses Protokoll ist jedem Mitglied mit der Einladung zur folgenden Mitgliederversammlung zuzusenden.

§ 10 Vorstand

1\. Der Vorstand besteht aus der/dem Vorsitzenden, der/dem StellvertreterIn, der/dem SchriftführerIn, der/dem KassiererIn und bis zu drei BeisitzerInnen. Seine Amtszeit beträgt zwei Jahre.  
2\. Der Vorstand darf nur aus natürlichen Personen als Einzelmitglied des Vereins bestehen.  
3\. Der Vorstand leitet den Verein und führt die laufenden Geschäfte des Vereines. Er ist an die Beschlüsse der Mitgliederversammlung gebunden.  
4\. Der Vorstand beschließt über alle Angelegenheiten im Rahmen der Vereinsziele, die nicht der Mitgliederversammlung vorbehalten sind.  
5\. Der Vorstand gibt sich eine eigene Geschäftsordnung.  
6\. Der Vorstand vertritt den Verein i.S. d. § 26 BGB. Zwei seiner Mitglieder, von denen einer der/die Vorsitzende oder der die stellvertretende Vorsitzende sein muss, sind gemeinsam vertretungsberechtigt.  
7\. Die Mitglieder des Vorstandes erhalten für ihre Tätigkeit keine Vergütung.  
8\. Die Amtszeit der Vorstandsmitglieder endet durch Neuwahl, durch Verlust der Mitgliedschaftsrechte, durch Abwahl oder Tod.

§ 11 Kassenrevision

Die Kasse des Vereines wird mindestens einmal jährlich durch zwei von der Mitgliederversammlung gewählte KassenrevisorInnen geprüft. Die KassenrevisorInnen erstatten der Mitgliederversammlung einen Prüfungsbericht und beantragen bei ordnungsgemäßer Führung der Kassengeschäfte die Entlastung des Vorstands.

§ 12 Auflösung des Vereines

1\. Die Auflösung des Vereines kann nur durch eine eigens hierzu einberufene Mitgliederversammlung beschlossen werden.  
2\. Die Auflösung des Vereines bedarf eines Beschlusses der Mitgliederversammlung mit einer Mehrheit von ¾ aller Anwesenden.  
3\. Bei Auflösung des Vereines oder Wegfall steuerbegünstigter Zwecke fällt sein Vermögen an eine juristische Person des öffentlichen Rechts oder eine andere steuerbegünstigte Körperschaft zwecks Verwendung für die Förderung des Völkerverständigungsgedankens.

§ 13 Inkrafttreten

Die Satzungänderungen wurde am 17. Juni 2006 von der Mitgliedervollversamlung beschlossen und treten  sofort in Kraft.

**Berlin Friedrichshain-Kreuzberg, den  17. Juni 2006**