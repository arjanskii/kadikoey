---
title: Spenden
date: 2018-09-09 16:29:11 +0000
excerpt: Mit Spenden können Sie die Arbeit unseres Vereins  unterstützen und uns helfen
  Projekte durchzuführen.

---
![Foto:privat](/upload/IMG_9751.JPG "Verteilung von Hilfsgütern aus Spendenmitteln ")

# Spenden

Als gemeinnütziger Verein sind wir “naturgemäß“ dankbar über jede Spende. Sie können damit die Arbeit unseres Vereins  unterstützen und uns helfen Projekte durchzuführen. Sie erhalten von uns eine steuerabsatzfähige Spendenbescheinigung.

**Unsere Kontoverbindung für Ihre Spenden**

Städtepartnerschaftsverein Kadiköy e.V.  
Postbank Leipzig  
IBAN DE62 8601 0090 0601 7239 02  
BIC PBNKDEFF

**Spendenaktion: Erdbeben im Golf von Iznik, 1999**

Nach dem Erdbeben im Golf von Iznik am 16.August 1999 hat der Verein zusammen mit dem Bezirk Kreuzberg neben vielen anderen aus dem Bezirk (Schulen, Projekte, Urbankrankenhaus etc.) Spenden gesammelt und den Bezirk Kadıköy in seinen Hilfsmaßnahmen vor Ort unterstützt. Von den Spendengeldern wurden Zelte, Medikamente, Bekleidung und Decken gekauft und vor Ort verteilt. Wir konnten garantieren, dass die Hilfen direkt den Erdbebenopfern zugute kamen.

Nach der großen ersten Hilfe haben wir eine kleine Hilfe in Form einer Patenschaft für Kinder aus Erdbebengebieten übernommen, die im Internat des Haydarpaşa- Gymnasiums untergebracht waren.

Sie erhielten über zwei Jahre ein monatliches Taschengeld aus den Spenden des Vereins im Rahmen der Kampagnen "Patenschaften für Erdbebenkinder" und  wurden  2000 nach Berlin eingeladen.

