---
title: Publikationen
date: 2018-10-23 13:15:21 +0200
excerpt: 'Schauen sie in unsere Broschüre.'

---
# Publikationen

### Broschüre

2017 haben wir unsere zweisprachige Broschüre in 2. Auflage herausgebracht. Sie können sie [hier herunterladen (PDF-Datei)](/upload/Kadikoey-eV-Broschüre.pdf).

### Faltblatt

Möchten Sie in unser Faltblatt schauen? Sie können es [hier herunterladen (PDF-Datei)](/upload/Kadikoey-eV-Faltblatt.pdf).