---
title: Verein
date: 1
excerpt: 'Der Städtepartnerschaftsverein wurde 1998 als gemeinnütziger Verein gegründet
  um die partnerschaftlichen Beziehungen durch Kontakte, Begegnungen und Informationen
  zwischen den Bürgerinnen und Bürgern beider Bezirke zu stärken und zu verbreiten.
  Viele unserer Mitglieder stammen aus der Türkei. '

---
# Verein

### Städtepartnerschaftsverein Kadıköy e.V.

Der Städtepartnerschaftsverein Kadıköy e.V. wurde 1998 gegründet um die partnerschaftlichen Beziehungen durch Kontakte, Begegnungen und Informationen zwischen den Bürger*innen beider Bezirke zu stärken und zu verbreiten.

Von unserem Selbstverständnis her versteht sich der Verein als „Brücke“. Wir möchten Begegnungen und Projekte zwischen Akteuren beider Bezirke fördern und begleiten. Dabei freuen uns über Partner, mit denen wir in längerfristig angelegten Projekten zusammenarbeiten können.

Wir arbeiten ehrenamtlich, sind gut in beiden Partnerbezirken vernetzt und arbeiten vertrauensvoll mit beiden Bezirksämtern zusammen. Für die Förderung der Städtepartnerschaftlichen Beziehungen erhalten wir Zuwendungen vom Bezirksamt Friedrichshain-Kreuzberg.

In unserer Arbeit bedeutet der große Begriff „Völkerverständigung“ in der ersten Linie Verstehen, Begegnung und Austausch zwischen Menschen aus zwei Kommunen zweier Länder.

Für Begegnungen und Austausch muss man „sich verstehen“ können. Daher verstehen wir uns auch als Sprach- und Kulturvermittlung. Die jüngere Generation der Türk*innen spricht und versteht erfreulicher Weise zunehmend gut Englisch. Dies erleichtert den direkten Kontakt zwischen den Menschen.

2018 wird der Verein 20 Jahren alt!