---
title: Chronik
date: 2018-09-09 16:26:37 +0000
excerpt: Chronik der Besuche und Begegnungen zwischen Friedrichshain-Kreuzberg und
  Kadιköy seit 1995

---
# Chronik

Besuche zwischen Friedrichshain-Kreuzberg und Kadιköy

**1995** Bürgermeister, Stadträte und Bezirksverordnete aus Kreuzberg besuchen Kadıköy.

**1996** Der Stellvertretende Bürgermeister, Stadträte und Bezirksverordnete aus Kadıköy besuchen Kreuzberg. Unterzeichnung der Städtepartnerschaft.

**1997** Bürgermeister, Bezirksverordnete, Schüler und Lehrer des Anadolu Güzel Sanatlar Lisesi besuchen Kreuzberg und die Partnerschule, die Carl-von-Ossietzky-Oberschule. Ausstellung über Kadıköy im Kreuzberg Museum. Bürgermeister, Stadträte und Bezirksverordnete aus Kreuzberg besuchen Kadıköy anlässlich des 75 .Gründungstags der Republik Türkei am 29.Oktober.

**1998** Der Bürgermeister von Kadıköy besucht mit Mitarbeitern der Verwaltung Abteilungen der Kreuzberger Verwaltung. Eine Schülerbegegnung findet statt. Bürgermeister, Stadträte und Bezirksverordnete aus Kreuzberg besuchen Kadıköy zum 76 .Gründungstag der türkischen Republik.

**1999** Großes Erdbeben am Golf von Izmit. Der Bezirk Kadıköy organisiert Erdbebenhilfe. Auch der Bezirk Kreuzberg leistet Erdbebenhilfe und unterstützt Kadiköy in seiner Hilfe. Zelte aus Kreuzberg stehen in den Lagern von Kırkpınar, Adapazarı, Düzce. Der Kreuzberger Bürgermeister, Bezirksverordnete und Städtepartnerschaftsvereinsmitglieder überbringen die Hilfsbereitschaft. Schülerbegegnung. Die Stellvertretende Bürgermeisterin İnci Beşpınar weilt in Kreuzberg, Solidaritätsveranstaltung für die Erdbebenhilfe.

FF![Foto:privat](/upload/Bild4.jpg "Zeltstadt ın Kırkpınar im Erdbebengebiet")

**2000** Erste Bildungs- und Begegnungsreise nach Kadıköy. Hilfe für Kinder aus Erdbebengebieten. Ehrenamtliche Mitarbeiterinnen von Sozialprojekten aus Kadıköy (KASDAV, Aile Danişma Merkezleri) besuchen Kreuzberg.

**2001** Zweite Bildungs- und Begegnungsreise (Sprachreise in Kooperation mit der VHS Friedrichshain-Kreuzberg) nach Kadıköy.

**2002** Ehrenamtliche Mitarbeiterinnen von Sozialprojekten aus Kadıköy (Aile Danişma Merkezleri, Kadıköy Frauenplattform) besuchen Friedrichshain-Kreuzberg.

**2003** Dritte und vierte Bildungsreise nach Kadıköy mit Mitarbeitern einer Kindertagesstätte und Sozial- und Beschäftigungsprojekten. Schüler der Robert-Koch- und der Erich-Fried-Oberschule besuchen Kadıköy. Die Bürgermeisterin con Friedrichshain-Kreuzberg der  BVV Vorsteher  im Begleitung von Kadiköy e.V besuchen anlässlich des 82. Gründungstags der Republik den Bezirk Kadıköy.

**2004** Ehrenamtliche Mitarbeiterinnen von BVV und Sozialprojekten aus Kadıköy (Behinderten-, Familien- und Frauenarbeit) besuchen Projekte in Friedrichshain-Kreuzberg. Stadträtin und Bezirksverordnete aus Friedrichshain-Kreuzberg besuchen Kadıköy.

**2005** Fünfte Bildungsreise nach Kadıköy und Teilnahme am ersten Behindertenkongress in Kadıköy mit Mitarbeitern aus Schule, Qualifizierungsprojekten, Behindertenarbeit sowie der Migrationsbeauftragten.

**2006** İnci Beşpınar und ehrenamtliche Mitarbeiterinnen von Sozialprojekten aus Kadıköy (Frauenhaus Kadıköy, Frauenplattform) besuchen Frauenprojekte in Berlin. Zehn Jahre Städtepartnerschafts-Jubiläum. Besuch einer offiziellen Delegation aus Kadıköy in Friedrichshain-Kreuzberg.

![Foto: privat](/upload/MYD060125137-b2048.jpg "Bezirksbürgermeister Cornelia Reinauer und die stellv.Bürgermeisterin İnci Beşpınar ")

**2007** Offizielle Jubiläumsfeier in Kadıköy. Besuch einer Delegation aus Berlin.

**2008** Projektreise aus Kadıköy für den Aufbau eines Rehabilitationszentrums in Kadıköy. Projektreise aus Friedrichshain-Kreuzberg, Kita-Erzieherinnen besuchen soziale Projekte in Kadıköy. Kontaktseminar der VHS Friedrichshain-Kreuzberg mit der VHS Kadıköy und der Aile Danişma Merkezleri.

**2009** Ausstellung: Das alte Istanbul in 3D. Juni/Juli 2009: Istanbul/Kadıköy  September/Oktober 2009 Berlin/ Friedrichshain-Kreuzberg- Kreuzbergmuseum  
Leitung und Mitarbeiter der Volkshochschule Kadıköy besuchen vom 10. bis 17.10 die Volkshochschule Friedrichshain- Kreuzberg

Kontaktseminar der Volkshochschule Kadıköy in Berlin mit der VHS Friedrichshain-Kreuzberg.

**2010** Ausstellung über Kadıköy in Berlin im Rathaus Charlottenburg

**2011** Fotoausstellung in Friedrichshain-Kreuzberg und Kadıköy: Schuhgröße 37- Frauenfußball in Ägypten der Türkei, Palästina und in Berlin anlässlich derFrauenfußball Weltmeisterschaft.

**2012** Der Bürgermeister Selami Öztürk von Kadıköy besucht mit Mitgliedern der BVV den Partnerbezirk.

Bildungsreise und Begegnungsreise zum Thema: Stadterneuerung / Sanierung in Istanbul am Beispiel von Fikirtepe / Kadıköy

Fotokurs zum Thema Stadtraum und Stadtentwicklung von Istanbul/ Kadıköy

Teilnahme an dem EU Workshop zur Verstärkung der Partnerschaft zwischen Städten und Bezirken der EU und der Türkei in Antalya

Berliner Ausstellung „Casa mare“ im Caddebostan Kültür Merkezi in Kadıköy

**2013** Bildungs-und Begegnungsreise des VAK e.V. Thema: Kindergärten und soziale Projekt in Kadıköy

„Exploring Istanbul   Ausstellung des Fotoprojekts „Stadtraum – Lebensraum Istanbul-Kadıköy.“ Ausstellung in Berlin und Kadıköy.

**2014** Verabschiedung des langjährigen Bürgermeisters von Kadıköy, Selami Öztürk gemeinsam mit ehem. Bürgermeister von Friedrichshain-Kreuzberg, Dr. Franz Schulz.

2\.Bildungs-und Begegnungsreise des VAK e.V. Thema: Kindergärten und soziale Projekt in Kadıköy.

Begrüßung des neuen Bürgermeisters von Kadıköy, Herrn Aykurt Nuhoğlu.

Fotoprojekt „Exploring Berlin“ mit türk. Fotografen in Berlin.

**2015** Ausstellung „Exploring Berlin“ im Baris Manco Kulturzentrum in Kadıköy

Bildungs- und Begegnungsreise Gesundheitsprävention und Betreuung von Kindern in Istanbul/ Kadıköy

Der Leistungskurs „Türkisch“ der Kreuzberger Robert Koch Oberschule besuchte während einer Projektfahrt den Partnerbezirk Kadıköy

Projektaufenthalt des Fotoprojektes „UrbanISTanbul in Kadıköy

**2016** Ausstellung „Exploring Berlin“ im Projektraum der Alten Feuerwache in Friedrichshain-Kreuzberg

Musikprojekt „Zelle 12: Dance classics“ mit Musikschülern der Partnerstädte Friedrichshain-

Kreuzberg Kadıköy, Stettin, Ingelheim an der Musikschule Friedrichshain-Kreuzberg

Projekt UrbanIStanbul / Ausstellung im Projektraum Bethanien

Jubiläumsfeier 20 Jahre Städtepartnerschaft mit Kadıköy und Stettin.

Ausstellung UrbanISTanbul im Caddebostan Kültür Merkezi in Kadıköy

![Foto: Anja Kozjot](/upload/IMG_7517.JPG "Ausstellung UrbanISTanbul in Caddebostan Kültür Merkezi")

**2017** Projekt „Partnerschaft in der Bildung“:11 Mitarbeiterinnen der 5 Kindertagesstätten und des Zentrums für Sozialberatung besuchen Kitas in Bezirk Friedrichshain Kreuzberg zum gemeinsamen Austausch.

Musikprojekt „Zelle 12“   World Talk: Die Musikschule Friedrichshain-Kreuzberg führt zum zweiten Mal das Musikprojekt durch mit Beteiligung der Musikschulen von Kadıköy, Stettin und Wiesbaden.