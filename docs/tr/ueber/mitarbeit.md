---
title: Mitarbeit
date: 2018-09-09 16:28:16 +0000
excerpt: Machen Sie mit bei kommunalen Begegnungsprojekten zwischen Friedrichshain-Kreuzberg
  und Kadıköy

---
# Mitarbeit

Möchten Sie mit uns arbeiten? Werden Sie Mitglied bei uns! Einfach hier [Aufnahmeantrag herunterladen]() (PDF-Datei), ausfüllen, abschicken.

Oder haben Sie Fragen, die Sie erläutert haben möchten? Wir melden uns umgehend bei Ihnen.