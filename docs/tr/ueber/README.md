# Hakkımızda

## Dernek
Kadıköy derneği 1998 yılında kuruldu. Derneğin amacı kardeş kent ilişkilerini her iki ilçenin vatandaşları, meslek grupları ve yerel yönetim birimleri arasında buluşmalar, bilgilenmeler ve ilişkiler yoluyla yaymak ve desteklemektir. Dernek bu görevi Friedrichshain-Kreuzberg ilçesi adına belediyeden alınan maddi bir destekle yürütmekte.
Kardeş kent derneği bu amaçlar çerçevesinde bir köprü olma görevini üzerlenir. Dernek her iki ilçe aktörleri arasında oluşan buluşmaları ve birlikte yürütülen projeleri destekler, onlara refakat eder. Karşılıklı ziyaretler yoluyla gerçekleşen tanışmalar, bilgi alış verişleri empati oluşumuna yol açmaktadır. Bu nedenle kardeş kent çalışmalarımız diller ve kültürlerarası iletişim küprüsü kurmaya olanak sağlıyor. Türkiye‘ de yetişen genç nesiller İngilizce’yı daha iyi konuştukları için kişilerarası oluşan ilişkiler daha kolay gelişmekte. Ve bu durum çeşitli avantajları da beraberinde getirmekte. 
Dernek yönetimi ve üyeleri çalışmaları fahri olarak ve kardeş kentlerle birlikte 20 yıldanberi yürütmektedir.

## Yönetim kurulu

**Unser Vorstand ist deutsch-türkisch:**

Christiane Zieger-Ayanoğlu  
Vorstandsvorsitzende

Özcan Ayanoğlu  
Stellvertretender Vorstandsvorsitzender

Peter Held  
Kassenwart

Ümit Bayam  
Schriftführer

Valie Djordjevic  
Beisitzerin

Metin Yilmaz  
Beisitzer


## Kronik
**Besuche zwischen Friedrichshain-Kreuzberg und Kadιköy**

**1995** Bürgermeister, Stadträte und Bezirksverordnete aus Kreuzberg besuchen Kadıköy.

**1996** Der Stellvertretende Bürgermeister, Stadträte und Bezirksverordnete aus Kadıköy besuchen Kreuzberg. Unterzeichnung der Städtepartnerschaft.

**1997** Bürgermeister, Bezirksverordnete, Schüler und Lehrer des Anadolu Güzel Sanatlar Lisesi besuchen Kreuzberg und die Partnerschule, die Carl-von-Ossietzky-Oberschule. Ausstellung über Kadıköy im Kreuzberg Museum. Bürgermeister, Stadträte und Bezirksverordnete aus Kreuzberg besuchen Kadıköy anlässlich des 75 .Gründungstags der Republik Türkei am 29.Oktober.

**1998** Der Bürgermeister von Kadıköy besucht mit Mitarbeitern der Verwaltung Abteilungen der Kreuzberger Verwaltung. Eine Schülerbegegnung findet statt. Bürgermeister, Stadträte und Bezirksverordnete aus Kreuzberg besuchen Kadıköy zum 76 .Gründungstag der türkischen Republik.

**1999** Großes Erdbeben am Golf von Izmit. Der Bezirk Kadıköy organisiert Erdbebenhilfe. Auch der Bezirk Kreuzberg leistet Erdbebenhilfe und unterstützt Kadiköy in seiner Hilfe. Zelte aus Kreuzberg stehen in den Lagern von Kırkpınar, Adapazarı, Düzce. Der Kreuzberger Bürgermeister, Bezirksverordnete und Städtepartnerschaftsvereinsmitglieder überbringen die Hilfsbereitschaft. Schülerbegegnung. Die Stellvertretende Bürgermeisterin İnci Beşpınar weilt in Kreuzberg, Solidaritätsveranstaltung für die Erdbebenhilfe.

FF![Foto:privat](/upload/Bild4.jpg "Zeltstadt ın Kırkpınar im Erdbebengebiet")

**2000** Erste Bildungs- und Begegnungsreise nach Kadıköy. Hilfe für Kinder aus Erdbebengebieten. Ehrenamtliche Mitarbeiterinnen von Sozialprojekten aus Kadıköy (KASDAV, Aile Danişma Merkezleri) besuchen Kreuzberg.

**2001** Zweite Bildungs- und Begegnungsreise (Sprachreise in Kooperation mit der VHS Friedrichshain-Kreuzberg) nach Kadıköy.

**2002** Ehrenamtliche Mitarbeiterinnen von Sozialprojekten aus Kadıköy (Aile Danişma Merkezleri, Kadıköy Frauenplattform) besuchen Friedrichshain-Kreuzberg.

**2003** Dritte und vierte Bildungsreise nach Kadıköy mit Mitarbeitern einer Kindertagesstätte und Sozial- und Beschäftigungsprojekten. Schüler der Robert-Koch- und der Erich-Fried-Oberschule besuchen Kadıköy. Die Bürgermeisterin con Friedrichshain-Kreuzberg der  BVV Vorsteher  im Begleitung von Kadiköy e.V besuchen anlässlich des 82. Gründungstags der Republik den Bezirk Kadıköy.

**2004** Ehrenamtliche Mitarbeiterinnen von BVV und Sozialprojekten aus Kadıköy (Behinderten-, Familien- und Frauenarbeit) besuchen Projekte in Friedrichshain-Kreuzberg. Stadträtin und Bezirksverordnete aus Friedrichshain-Kreuzberg besuchen Kadıköy.

**2005** Fünfte Bildungsreise nach Kadıköy und Teilnahme am ersten Behindertenkongress in Kadıköy mit Mitarbeitern aus Schule, Qualifizierungsprojekten, Behindertenarbeit sowie der Migrationsbeauftragten.

**2006** İnci Beşpınar und ehrenamtliche Mitarbeiterinnen von Sozialprojekten aus Kadıköy (Frauenhaus Kadıköy, Frauenplattform) besuchen Frauenprojekte in Berlin. Zehn Jahre Städtepartnerschafts-Jubiläum. Besuch einer offiziellen Delegation aus Kadıköy in Friedrichshain-Kreuzberg.

![Foto: privat](/upload/MYD060125137-b2048.jpg "Bezirksbürgermeister Cornelia Reinauer und die stellv.Bürgermeisterin İnci Beşpınar ")

**2007** Offizielle Jubiläumsfeier in Kadıköy. Besuch einer Delegation aus Berlin.

**2008** Projektreise aus Kadıköy für den Aufbau eines Rehabilitationszentrums in Kadıköy. Projektreise aus Friedrichshain-Kreuzberg, Kita-Erzieherinnen besuchen soziale Projekte in Kadıköy. Kontaktseminar der VHS Friedrichshain-Kreuzberg mit der VHS Kadıköy und der Aile Danişma Merkezleri.

**2009** Ausstellung: Das alte Istanbul in 3D. Juni/Juli 2009: Istanbul/Kadıköy  September/Oktober 2009 Berlin/ Friedrichshain-Kreuzberg- Kreuzbergmuseum  
Leitung und Mitarbeiter der Volkshochschule Kadıköy besuchen vom 10. bis 17.10 die Volkshochschule Friedrichshain- Kreuzberg

Kontaktseminar der Volkshochschule Kadıköy in Berlin mit der VHS Friedrichshain-Kreuzberg.

**2010** Ausstellung über Kadıköy in Berlin im Rathaus Charlottenburg

**2011** Fotoausstellung in Friedrichshain-Kreuzberg und Kadıköy: Schuhgröße 37- Frauenfußball in Ägypten der Türkei, Palästina und in Berlin anlässlich derFrauenfußball Weltmeisterschaft.

**2012** Der Bürgermeister Selami Öztürk von Kadıköy besucht mit Mitgliedern der BVV den Partnerbezirk.

Bildungsreise und Begegnungsreise zum Thema: Stadterneuerung / Sanierung in Istanbul am Beispiel von Fikirtepe / Kadıköy

Fotokurs zum Thema Stadtraum und Stadtentwicklung von Istanbul/ Kadıköy

Teilnahme an dem EU Workshop zur Verstärkung der Partnerschaft zwischen Städten und Bezirken der EU und der Türkei in Antalya

Berliner Ausstellung „Casa mare“ im Caddebostan Kültür Merkezi in Kadıköy

**2013** Bildungs-und Begegnungsreise des VAK e.V. Thema: Kindergärten und soziale Projekt in Kadıköy

„Exploring Istanbul   Ausstellung des Fotoprojekts „Stadtraum – Lebensraum Istanbul-Kadıköy.“ Ausstellung in Berlin und Kadıköy.

**2014** Verabschiedung des langjährigen Bürgermeisters von Kadıköy, Selami Öztürk gemeinsam mit ehem. Bürgermeister von Friedrichshain-Kreuzberg, Dr. Franz Schulz.

2\.Bildungs-und Begegnungsreise des VAK e.V. Thema: Kindergärten und soziale Projekt in Kadıköy.

Begrüßung des neuen Bürgermeisters von Kadıköy, Herrn Aykurt Nuhoğlu.

Fotoprojekt „Exploring Berlin“ mit türk. Fotografen in Berlin.

**2015** Ausstellung „Exploring Berlin“ im Baris Manco Kulturzentrum in Kadıköy

Bildungs- und Begegnungsreise Gesundheitsprävention und Betreuung von Kindern in Istanbul/ Kadıköy

Der Leistungskurs „Türkisch“ der Kreuzberger Robert Koch Oberschule besuchte während einer Projektfahrt den Partnerbezirk Kadıköy

Projektaufenthalt des Fotoprojektes „UrbanISTanbul in Kadıköy

**2016** Ausstellung „Exploring Berlin“ im Projektraum der Alten Feuerwache in Friedrichshain-Kreuzberg

Musikprojekt „Zelle 12: Dance classics“ mit Musikschülern der Partnerstädte Friedrichshain-

Kreuzberg Kadıköy, Stettin, Ingelheim an der Musikschule Friedrichshain-Kreuzberg

Projekt UrbanIStanbul / Ausstellung im Projektraum Bethanien

Jubiläumsfeier 20 Jahre Städtepartnerschaft mit Kadıköy und Stettin.

Ausstellung UrbanISTanbul im Caddebostan Kültür Merkezi in Kadıköy

![Foto: Anja Kozjot](/upload/IMG_7517.JPG "Ausstellung UrbanISTanbul in Caddebostan Kültür Merkezi")

**2017** Projekt „Partnerschaft in der Bildung“:11 Mitarbeiterinnen der 5 Kindertagesstätten und des Zentrums für Sozialberatung besuchen Kitas in Bezirk Friedrichshain Kreuzberg zum gemeinsamen Austausch.

Musikprojekt „Zelle 12“   World Talk: Die Musikschule Friedrichshain-Kreuzberg führt zum zweiten Mal das Musikprojekt durch mit Beteiligung der Musikschulen von Kadıköy, Stettin und Wiesbaden.



## Tüzük

**Satzung des "Städtepartnerschaftsverein Kadıköy e.V."**

§ 1 Name und Sitz

1\. Der Verein führt den Namen "Städtepartnerschaftsverein Kadiköy e.V.".  
2\. Der Sitz des Vereins ist Berlin  
3\. Der Verein ist in das Vereinsregister des Amtsgerichtes Charlottenburg von Berlin eingetragen.

§ 2 Zweck des Vereins

1\. Der Zweck des Vereines ist die Förderung des Austausches zwischen den Einwohnern und Freunden der Gemeinden Berlin - Friedrichshain-Kreuzberg und Istanbul - Kadiköy insbesondere zu gesellschaftspolitischen und kulturellen Belangen mit dem Ziel der Erhaltung des Friedens und der Völkerverständigung. Hauptziel ist die Förderung der internationalen Gemeinschaft und der Toleranz auf allen Gebieten des Verständigungsgedankens.  
2\. Der Satzungszweck wird insbesondere verwirklicht durch  
 - die Verbreitung von Informationen über Kadiköy bzw. dort über Friedrichshain-Kreuzberg  
 - die Organisation von Begegnungen zwischen Menschen aus Friedrichshain-Kreuzberg und Kadiköy  
 - durch die Organisation von Veranstaltungen für den Austausch über Geschichte, Kultur, Stadtentwicklung und soziale Fragen  
 - Unterstützung der Erdbebenopfer  
3\. Der Verein ist überparteilich und überkonfesssionell.

§ 3 Gemeinnützigkeit

1\. Der Verein verfolgt ausschließlich und unmittelbar diese gemeinnützigen Zwecke im Sinne des Abschnittes "Steuerbegünstigte Zwecke" der Abgabenordnung.  
2\. Der Verein ist selbstlos tätig und verfolgt nicht eigenwirtschaftliche Zwecke.  
3\. Mittel des Vereines dürfen nur für satzungsmäßige Zwecke verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.  
4\. Es darf keine Person durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

§ 4 Mitgliedschaft

1\. Mitglieder des Vereines können natürliche und juristische Personen werden.  
2\. Neben den Mitgliedern gibt es Fördermitglieder, die ausschließlich den Verein in seiner Arbeit gemäß § 2 fördern. Diesen Fördermitgliedern stehen nicht die Rechte aus § 8 zu. Ihre Aufnahme erfolgt durch schriftliche Erklärung.  
3\. Die Mitgliedschaft wird durch schriftliche Erklärung gegenüber dem Vorstand beantragt. Der Eintritt erfolgt über Annahme durch de Vorstand. Die Mitgliederversammlung wird über alle Anträge und aufnahmen informiert.  
4\. Die Mitgliedschaft erlischt durch Austritt, Ausschluss oder Tod.  
5\. Der Austritt ist schriftlich mit einer Frist von vier Wochen zum Ende eines Quartals gegenüber dem Vorstand zu erklären.  
6\. Über den Ausschluss eines Mitgliedes entscheidet die Mitgliederversammlung.  
7\. Ist ein Mitglied mit mindestens einem Jahresbeitrag trotz einmaliger schriftlicher Zahlungsaufforderung im Rückstand, ruhen seine Mitgliedsrechte. Die Mitgliedschaft erlischt, ohne daß es eines besonderen Beschlusses der Mitgliederversammlung bedarf, wenn das Mitglied trotz einmaliger, und von Satz 1 unabhängiger schriftlicher Mahnung mit zwei Jahresbeiträgen im Rückstand ist. Das Mitglied ist auf die Folgen hinzuweisen.

§ 5 Finanzen

Die finanziellen Mittel des Vereines setzen sich aus Mitgliedsbeiträgen, privaten Spenden und aus Zuwendungen der öffentlichen Hand zusammen. Über die Annahme von Spenden entscheidet der Vorstand. Die Mitgliederversammlung wird über alle angebotenen und angenommenen Spenden informiert.

§ 6 Mitgliedsbeiträge

1\. Jedes Mitglied ist beitragspflichtig  
Die Höhe der Beiträge und die Fälligkeit werden durch den Vorstand  festgelegt.

§ 7 Geschäftsjahr

Das Geschäftsjahr ist das Kalenderjahr.

§ 8 Organe

Organe des Vereines sind die Mitgliederversammlung und der Vorstand.

§ 9 Mitgliederversammlung

1\. Die Mitgliederversammlung ist in allen Fragen oberstes Beschlussorgan und umfasst alle Mitglieder des Vereines  
2\. .Jedes Mitglied hat eine Stimme. Vertreter juristischer Personen können nicht gleichzeitig als einfache Mitglieder abstimmen.  
3\. Die ordentliche Mitgliederversammlung ist mindestens einmal im Geschäftsjahr einzuberufen. sie ist schriftlich unter Mitteilung der Tagesordnung mit einer Frist von 14 Tagen vom Vorstand einzuberufen.  
4\. Die Mitgliederversammlung ist beschlussfähig, wenn sie ordnungsgemäß einberufen wurde.  
5\. Außerordentliche Mitgliederversammlungen werden auf Beschluss des Vorstandes oder auf schriftlich begründeten Antrag von mindestens 1/10 der Mitglieder einberufen.  
6\. Beschlüsse der Mitgliederversammlung bedürfen der einfachen Mehrheit, Satzungsänderungen einer Mehrheit von 2/3 der abgegebenen Stimmen. Stimmenthaltungen werden bei der Berechnung der Mehrheit nicht mitgezählt. Der Ausschluss eines Mitgliedes bedarf der Mehrheit von 2/3 der Anwesenden. Jeder Antrag auf Ausschluss eines Mitgliedes ist in der Einladung als Tagesordnungspunkt kenntlich zu machen..  
7\. Die Mitgliederversammlung entscheidet über Ziele, Aufgaben und Strukturen des Vereines. Sie beschließt über die Jahresabrechnung, die Entlastung des Vorstandes und der KassenprüferInnen. Sie beschließt den Ausschluss von Mitgliedern  
8\. .Die Mitgliederversammlung wählt den Vorstand, die BeisitzerInnen sowie zwei KassenprüferInnen.  
9\. Die Mitgliederversammlung hat das Recht, Mitglieder des Vorstandes während der laufenden Amtsperiode abzuwählen. Erforderlich ist eine Mehrheit von 2/3 der Anwesenden.  
10\. Über die Mitgliederversammlung ist ein Protokoll zu führen, das von der/dem VersammlungsleiterIn und der/dem Schriftführer abzuzeichnen ist. Dieses Protokoll ist jedem Mitglied mit der Einladung zur folgenden Mitgliederversammlung zuzusenden.

§ 10 Vorstand

1\. Der Vorstand besteht aus der/dem Vorsitzenden, der/dem StellvertreterIn, der/dem SchriftführerIn, der/dem KassiererIn und bis zu drei BeisitzerInnen. Seine Amtszeit beträgt zwei Jahre.  
2\. Der Vorstand darf nur aus natürlichen Personen als Einzelmitglied des Vereins bestehen.  
3\. Der Vorstand leitet den Verein und führt die laufenden Geschäfte des Vereines. Er ist an die Beschlüsse der Mitgliederversammlung gebunden.  
4\. Der Vorstand beschließt über alle Angelegenheiten im Rahmen der Vereinsziele, die nicht der Mitgliederversammlung vorbehalten sind.  
5\. Der Vorstand gibt sich eine eigene Geschäftsordnung.  
6\. Der Vorstand vertritt den Verein i.S. d. § 26 BGB. Zwei seiner Mitglieder, von denen einer der/die Vorsitzende oder der die stellvertretende Vorsitzende sein muss, sind gemeinsam vertretungsberechtigt.  
7\. Die Mitglieder des Vorstandes erhalten für ihre Tätigkeit keine Vergütung.  
8\. Die Amtszeit der Vorstandsmitglieder endet durch Neuwahl, durch Verlust der Mitgliedschaftsrechte, durch Abwahl oder Tod.

§ 11 Kassenrevision

Die Kasse des Vereines wird mindestens einmal jährlich durch zwei von der Mitgliederversammlung gewählte KassenrevisorInnen geprüft. Die KassenrevisorInnen erstatten der Mitgliederversammlung einen Prüfungsbericht und beantragen bei ordnungsgemäßer Führung der Kassengeschäfte die Entlastung des Vorstands.

§ 12 Auflösung des Vereines

1\. Die Auflösung des Vereines kann nur durch eine eigens hierzu einberufene Mitgliederversammlung beschlossen werden.  
2\. Die Auflösung des Vereines bedarf eines Beschlusses der Mitgliederversammlung mit einer Mehrheit von ¾ aller Anwesenden.  
3\. Bei Auflösung des Vereines oder Wegfall steuerbegünstigter Zwecke fällt sein Vermögen an eine juristische Person des öffentlichen Rechts oder eine andere steuerbegünstigte Körperschaft zwecks Verwendung für die Förderung des Völkerverständigungsgedankens.

§ 13 Inkrafttreten

Die Satzungänderungen wurde am 17. Juni 2006 von der Mitgliedervollversamlung beschlossen und treten  sofort in Kraft.

**Berlin Friedrichshain-Kreuzberg, den  17. Juni 2006**



## İşbirliği
Möchten Sie mit uns arbeiten? Werden Sie Mitglied bei uns! Einfach hier [Aufnahmeantrag herunterladen (PDF-Datei)](Kadikoey-eV-Berlin-Aufnahmeformular.pdf), ausfüllen, abschicken.

Oder haben Sie Fragen, die Sie erläutert haben möchten? Wir melden uns umgehend bei Ihnen.


## Bağış

![Foto:privat](/upload/IMG_9751.JPG "Verteilung von Hilfsgütern aus Spendenmitteln ")

**Mit Spenden können Sie die Arbeit unseres Vereins  unterstützen und uns helfen Projekte durchzuführen.**

Als gemeinnütziger Verein sind wir “naturgemäß“ dankbar über jede Spende. Sie können damit die Arbeit unseres Vereins  unterstützen und uns helfen Projekte durchzuführen. Sie erhalten von uns eine steuerabsatzfähige Spendenbescheinigung.

**Unsere Kontoverbindung für Ihre Spenden**

Städtepartnerschaftsverein Kadiköy e.V.  
Postbank Leipzig  
IBAN DE62 8601 0090 0601 7239 02  
BIC PBNKDEFF

**Spendenaktion: Erdbeben im Golf von Iznik, 1999**

Nach dem Erdbeben im Golf von Iznik am 16.August 1999 hat der Verein zusammen mit dem Bezirk Kreuzberg neben vielen anderen aus dem Bezirk (Schulen, Projekte, Urbankrankenhaus etc.) Spenden gesammelt und den Bezirk Kadıköy in seinen Hilfsmaßnahmen vor Ort unterstützt. Von den Spendengeldern wurden Zelte, Medikamente, Bekleidung und Decken gekauft und vor Ort verteilt. Wir konnten garantieren, dass die Hilfen direkt den Erdbebenopfern zugute kamen.

Nach der großen ersten Hilfe haben wir eine kleine Hilfe in Form einer Patenschaft für Kinder aus Erdbebengebieten übernommen, die im Internat des Haydarpaşa- Gymnasiums untergebracht waren.

Sie erhielten über zwei Jahre ein monatliches Taschengeld aus den Spenden des Vereins im Rahmen der Kampagnen "Patenschaften für Erdbebenkinder" und  wurden  2000 nach Berlin eingeladen.



## Publikationen
### Broschüre
2017 haben wir unsere zweisprachige Broschüre in 2. Auflage herausgebracht. Sie können sie [hier herunterladen (PDF-Datei)](/upload/Kadikoey-eV-Broschüre.pdf).

### Faltblatt
Möchten Sie in unser Faltblatt schauen? Sie können es [hier herunterladen (PDF-Datei)](/upload/Kadikoey-eV-Faltblatt.pdf).


## Iletişim

Treten Sie mit uns in Kontakt!

[Email](mailto:kadikoey.ev@web.de)
