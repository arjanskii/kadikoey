---
title: taz-Artikel über die Städtepartnerschaft
date: 2018-08-13 19:34:18 +0000
excerpt: Die Berliner Tageszeitung taz hat am 07. Januar ausführlich über die Situation
  in der Türkei und die Rolle der Städtepartnerschaften berichtet.
position: 7
thumbnail: "/upload/Meldung_taz.jpeg"

---
Der [Artikel "Unter Druck" von Elisabeth Kimmerle](http://www.taz.de/!5368701/ "taz Artikel Unter Druck von Elisabeth Kimmerle") mit der Einleitung "Terror und Repression in der Türkei – das hat auch Auswirkungen auf die Städtepartnerschaft zwischen Berlin und Istanbul. Es kriselt. Und man rückt etwas enger zusammen" wurde am 07.01.2017 im taz.am Wochende veröffentlicht und ist im Internet abrufbar.

[Artikel als PDF-Datei](https://app.forestry.io/sites/lxsfifezxvi0ya/body-media//upload/Artikel_taz.pdf "Artikel als PDF-Datei")