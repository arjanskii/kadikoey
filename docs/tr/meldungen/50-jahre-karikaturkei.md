---
title: "Ausstellung: 50 Jahre Kariakatürkei"
date: 2018-09-07 13:47:17 +0000
excerpt: "Wir verrecken vor Lachen! - 50 Jahre Karikatürkei
  Ausstellung, Lecture, Performance, Stand Up Talks, Workshops
  8.SEP-4.NOV 2018
  Kunstraum Kreuzberg/Bethanien"
position: 8
thumbnail: "/upload/Meldung_Karikatuerkei.jpg"

---
### Wir verrecken vor Lachen!- 50 Jahre Karikatürkei

Ausstellung, Lecture, Performance, Stand Up Talks, Workshops  
8.September - 4.November 2018  
Kunstraum Kreuzberg/Bethanien

Weitere Informationen und Programm [finden Sie hier](http://www.kunstraumkreuzberg.de/start.html).

![](/upload/KarikaAfis03.jpg)