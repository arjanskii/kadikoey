---
title: Friedrichshain-Kreuzberg
subtitle: 281.323 Einwohner (2016)
date: 2018-08-12 10:46:46 +0000
excerpt: Friedrichshain-Kreuzberg verbindet zwei Innenstadtbezirke, die 40 Jahre durch
  die Berliner Mauer getrennt waren und jeweils dem Ostteil der Stadt (Friedrichshain)
  und dem Westteil der Stadt (Kreuzberg) angehörten.
thumbnail: "/upload/Header_Fr-Kreuzberg.jpg"

---
# Friedrichshain-Kreuzberg

<img src="/upload/Header_Fr-Kreuzberg.jpg" alt="Oberbaumbrücke, Berlin"/>

## Friedrichshain-Kreuzberg verbindet zwei Innenstadtbezirke, die 40 Jahre durch die Berliner Mauer getrennt waren und jeweils dem Ostteil der Stadt (Friedrichshain) und dem Westteil der Stadt (Kreuzberg) angehörten.

Der Bezirk ist ein echter Metropolenbezirk mit lebhaften Straßenzügen, vielen Restaurants und vor allem mit viel Kunst und Kultur. Die Oberbaumbrücke, das Wahrzeichen des Bezirks, verbindet beide Bezirke und ist zugleich Austragungsort des zweimal jährlich stattfindenden Kunst Festivals, der OpenAir Gallery.  Die Bergmannstraße, Oranienstraße in Kreuzberg und die Straßen am Berliner Ostbahnhof und die RAW-Gelände in Friedrichshain sind bis in die späte Nacht belebt. Junge Menschen in vielen Sprachen, aus allen europäischen Ländern machen diese Stadtbereiche zu internationalen, attraktiven Begegnungsorten.

<KarteFr-Kr />

Im kulturellen Bereich kann der Bezirk mit vielen Museen, Galerien, Theatern und Spielstätten aus der offiziellen und alternativen Kulturscene punkten. z.B. der „Martin-Gropius-Bau“ die „Berlinische Galerie“, die „Hau-Theater“, „Jüdisches Museum“, und „Technikmuseum“, Die „Mercedes-Benz-Arena“ ist einer der größten Veranstaltungsorte Berlins. Der Bezirk selber betreibt zahlreiche Kultur und Kunsteinrichtungen wie das Bezirksmuseum, Galerien oder die Musikschule in historischen Gebäuden des Bezirks. Auch für temporäre kulturelle Nutzung finden sich immer noch freie Orte. Trotz seiner Innenstadtlage und Bevölkerungsdichte ist Friedrichshain- Kreuzberg ein „grüner Bezirk“ mit vielen Parks, Spielplätzen, Grünzügen, Baumanpflanzungen und begrünten Hinterhöfen.

### Ehemals Arbeiterbezirke…

Vor dem zweiten Weltkrieg waren beide Bezirke Arbeiterbezirke mit vielen Industrie- und Gewerbetrieben. Seit den 80 und 90 Jahren mit dem Abbau der Industriearbeitsplätze haben Gründungen im Bereich neuer Medien, Design, Musikproduktion etc. diese Betriebe ersetzt.

### Seit den 1960er Jahren Einwanderung von Menschen aus der Türkei…

Der Bezirksteil Kreuzberg ist seit den 1960er Jahren durch die Einwanderung von Menschen aus der Türkei geprägt worden. Sie kamen als Arbeitsimmigranten, als Studenten und als politische Flüchtlinge nach dem Putsch vom 12.September 1980. Sie gehören zu Kreuzberg und zu Berlin und prägen das Stadtbild und das kulturelle Leben. Allerdings ist der Bezirk Friedrichshain-Kreuzberg heute auch ein europäisch- internationaler Bezirk. Deutsch, Englisch, Spanisch, Französisch sind heute neben dem Türkischen die häufigsten Sprachen in den Straßen.

### Internationalität und rasanter Wandel…

Für die Bevölkerung des Bezirks bringt der Wandel vom Randbezirk zum pulsierenden Innenstadtbezirk neue Herausforderungen. Die Mieten steigen stetig, ehemals günstige Mietwohnungen werden vermehrt in Eigentumswohnungen umgewandelt, Mieter aus ihren Wohnungen und Wohnquartieren verdrängt.

Doch die Friedrichshain-Kreuzberger Bürger sind als wiederständig bekannt. Dort wo Veränderungen als ungerecht, unsozial oder unökologisch erlebt werden, engagieren sie sich und bringen Ihre Vorschläge ein.

### Politisch links alternativ…

Politisch ist der Bezirk seit Jahren eher links aufgestellt. Im Bezirksparlament sind vertreten: Bündnis 90/Die Grünen (20), Die Linken (12), die SPD (10), CDU (4), die AFD (3), Die Piraten (2), die FDP (2) , und Die Partei (2). (Reihenfolge nach Anzahl der Sitze). Seit 2006 stellen die Grünen den Bürgermeister/die Bürgermeisterin. Das Bezirksamt ist jedoch ein Kollektivorgan, und wird entsprechend der Wahlergebnisse von den stärksten Parteien gebildet. Grüne (3 Stadträte incl. Bürgermeisterin) Linke (1 Stadtrat), SPD (1 Stadtrat).

<div class="box">

<img style="width:100px; display: block; margin-left: auto; margin-right: auto" src="/upload/wappen_friedrichshain.gif">

### Friedrichshain-Kreuzberg in Zahlen

2016 zählte der Bezirk Friedrichshain-Kreuzberg 281.323 Einwohner. Der Anteil der Bevölkerung mit Migrationshintergrund beläuft sich auf 41 Prozent (115.370 Einwohner)
[Quelle: Amt für Statistik Berlin-Brandenburg](http://www.berlin.de/ba-friedrichshain-kreuzberg/)

### Nach der Fusion

Der Bezirk Friedrichshain-Kreuzberg ist der 2. Verwaltungsbezirk von Berlin, der aus der Fusion der bisherigen Bezirke Friedrichshain (ehemals Ostteil der Stadt) und Kreuzberg (ehemals Westteil der Stadt) entstanden ist.

Die beiden Ortsteile des Bezirks, Kreuzberg und Friedrichshain, sind durch die Spree von einander getrennt. Die Oberbaumbrücke verbindet beide Alt-Bezirke und ist damit zum Wahrzeichen des neuen Verwaltungsbezirks geworden. Im Wappen ist die Brücke, die Spree und der Berliner Bär zu erkennen.

</div>
