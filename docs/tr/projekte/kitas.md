---
title: Kindertagesstätten
date: 2017-02-04 00:00:00 +0000
thumbnail: "/upload/Header_Kita.jpg"
year: 2017
categories:
- Kindertagesstätten
description: Wie werden Kinder in Friedrichshain-Kreuzberg und Kadıköy groß, welche
  Aufgaben erfüllen die Kindertagesstätten?

---
![Foto: privat](/upload/Header_Kita.jpg "Erzieherinen aus Kadiköy und Kreuzberg")

# Kindertagesstätten

### Besuche von Kindertagesstätten

Wie werden Kinder in Friedrichshain-Kreuzberg und Kadıköy groß, welche Aufgaben erfüllen die Kindertagesstätten? Seit vielen Jahren gibt es ein großes Interesse von Kindertagesstätten aus Friedrichshain-Kreuzbergs Bildungs-und Sozialeinrichtungen in Kadıköy kennen zu lernen. Zwischen 2003 und 2014 haben wir allein fünf Bildungsreisen mit Mitarbeiter*innen von Kindertagesstätten durchgeführt.

### TANDEM-Austausch zwischen Kindertagesstätten

2013 begann ein intensiverer Austausch zwischen den Friedrichshain-Kreuzberger VAK e.V. Kitas und den 5 Kindertagesstäten des Bezirksamtes Kadıköy. Nach gegenseitigen Besuchen folgt nun 2018 eine TANDEM-Phase, in der jeweils 3 Erzieher_innen für eine Woche in den Kitas des Partnerbezirkes am Gruppenalltag teilnehmen werden. Sie lernen so in der Praxis die pädagogischen Schwerpunkte der anderen Kita kennen und können diese Ansätze reflektieren und sie in Beziehung zur eigenen pädagogischen Arbeit setzten. Sie können Ihre Erfahrungen einbringen und den Erzieher_innen Anregungen vorschlagen. Neben dem Interesse am fachlichen Austausch prägen Neugier und Offenheit für das Kennenlernen kultureller Vielfalt diesen Austausch.

Der Kita-Tandem Austausch findet in Berlin vom 3. bis 9.Juni 2018 und in Istanbul vom 11. bis 17. November 2018 statt.

<div class="box">

### Kontakt Kindertagesstätten der VAK-Kitas e.V.

Geschäftsstelle:  
Oranienstr. 2a  
10997 Berlin  
Tel. 030 - 618 65 74  
Fax. 611 70 47   
Mail:VAK-Kitas@t-online.de    
Internet: www.vak-kindertagesstaetten.de

Kindertagesstätten:  
Oranienstr. 4  
10997 Berlin  
Tel. 618 63 19  
Reichenberger Str. 156 a  
10999 Berlin  
Tel. 610 76 668

### Kontakt Kindertagesstätten des Bezirksamts Kadıköy

Internet: [www.cocukyuvalari.kadikoy.bel.tr/](http://www.cocukyuvalari.kadikoy.bel.tr/)

Kindertagesstätten:

İsmail Hakkı Tonguç Çocuk Yuvası  Merdivenköy/ Kadıköy  
Hasan Ali Yücel Çocuk Yuvası , Kozyatağı/Kadıköy  
Mevhibe Inönü Çocuk Yuvası,  Söğütlüçeşme /Kadıköy  
Fenerbahçe Çocuk Yuvası ,  Fenerbahçe /Kadıköy  
Bahriye Üçok Ekolojik Çocuk Yuvası  Sahrayı Cedid/Kadıköy

</div>