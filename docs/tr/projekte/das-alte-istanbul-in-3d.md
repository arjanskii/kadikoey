---
title: Das alte Istanbul in 3D
date: 2018-08-12 09:25:53 +0000
thumbnail: "/upload/DSC_9854.JPG"
description: Stereoskopische Fotografien über Istanbul vor dem 1. Weltkrieg aus dem
  so genanntem „ Kaiserpanorama“ des Fotografen August Fuhrmann (1844-1925).
categories:
- Ausstellung
- Istanbul
- Fotografie
year: 2010

---
![](/upload/DSC_9854.JPG)

# Das alte Istanbul in 3D

### Konstantinopel in stereoskopischen Fotografien aus dem Berliner Kaiserpanorama

Fast 100 Jahre nach ihrer Entstehung präsentierte die Ausstellung „ Das alte Istanbul in 3D“ stereoskopische Fotografien über Istanbul vor dem 1. Weltkrieg aus dem so genanntem „ Kaiserpanorama“ des Fotografen August Fuhrmann (1844-1925).

Die Ausstellung führt die Besucher auf eine besondere Zeitreise durch das Istanbul um 1912. Der 3 D Effekt und die Farbigkeit sorgen für eine eigenartige Präsenz und besondere Faszination. Die Betrachter können in die Tiefe der Bilder versinken, die Menschen auf den Fotographien wirken lebendig und dadurch sehr nah.

Zu sehen sind Stadtansichten, auch von nicht mehr existierenden Gebäuden, Alltags- und Straßenszenen, offizielle Rituale und Truppenparaden, außerdem Ausflugsorte auf den Prinzeninseln und am Bosporus. Angefertigt wurden diese Bilder für die so genannten „Kaiserpanoramen“ des Fotografen August Fuhrmann (in Berlin in der Friedrichstraße), die neben Bildberichten zum aktuellen einheimischen Geschehen Ausblicke in die „Weite Welt“ eröffneten und als Vorläufer der Wochenschauen das damals wohl beliebteste Massenmedium waren.

### "Kaiserpanorama" von August Fuhrmann

In einem Kaiserpanorama konnten 25 Personen gleichzeitig thematisch gegliederte Bilderserien von jeweils 50 Bildern betrachten. Es wurden Themenabende veranstaltet z.B. über die Reise Wilhelm II. nach Sofia und Konstantinopel. Zur Blütezeit dieses Mediums gab es 250 Filialen des Kaiserpanoramas (später Weltpanorama genannt) in Deutschland, der Schweiz und Österreich.

![](/upload/DSC00571.JPG)

### 20 Jahre Jubiläum Berlin - Istanbul

Das 20ste Jubiläum der Partnerschaft zwischen Berlin und Istanbul und die Partnerschaft zwischen dem Istanbuler Bezirk Kadıköy und dem Berliner Bezirk Friedrichshain-Kreuzberg waren der Anlass diese Bilder in einer Ausstellung nach Istanbul zu bringen. Der Städtepartnerschaftsverein Kadıköy e.V. und das Bezirksmuseum FriedrichshainKreuzberg entwickelten die Idee und stellten die einhundert Konstantinopel Fotografien erstmals zu einer Ausstellung zusammen. Während der „ berlin day´s“ in Istanbul wurde die Ausstellung am 20. Juni in Istanbul durch Vertreter des Berliner Senats und des Bezirksamt Kadiköy eröffnet und bis zum 30.Juli der Istanbuler Öffentlichkeit gezeigt. Durch die Unterstützung des Bezirksamts Kadiköy konnte die Ausstellung im Gebäude des historischen Rathauses, dem Sehremaneti Belediye Binası, gezeigt werden.

Die Ausstellung umfasst hundert handkolorierte stereoskopische Fotografien, davon 50 in einer permanenten 3D-Dia Schau auf Spezialleinwand. Für die Erzielung des 3D-Effektes erhält das Publikum spezielle Brillen. Achtundvierzig weitere Bilder werden in Spezialbetrachtungsgeräten mit Okkularen präsentiert. Zehn besonders schöne und aussagekräftige Fotografien sind als Rahmung der Ausstellung auf das Format 100x100cm vergrößert. Ein nach empfundenes „Kaiserpanorama“ im Zehneck präsentiert sechs weitere stereoskopische Fotografien in Einzelbildbetrachtern im Orginalformat mit 3D Effekt. Texttafeln erläutern den zeitgeschichtlichen (preußisch - osmanische Beziehungen) und fotografiegeschichtlichen Kontext.

Neben den Fotografien aus Istanbul wurden alte Ansichten aus Kreuzberg in 3 D Format gezeigt, Bilder und Texte dokumentieren die 13jährige Partnerschaft zwischen dem Istanbuler Bezirk Kadıköy und Friedrichshain–Kreuzberg.

Nach Beendigung der Ausstellung in Istanbul wurde die Ausstellung nach Berlin zurückgebracht und am 29. August im Kreuzberg Museum zur Langen Nacht der Museen durch das Bezirksamtes Friedrichshain- Kreuzberg und den Generalkonsul der Republik der Türkei in Berlin eröffnet. Während der Langen Nacht wurde die 3D –Diashow über das alte Istanbul von dem Stummfilmpianisten Stephan v. Bothmer begleitet. Die Ausstellung dauerte mit Verlängerung bis zum 25.Oktober 2009 und war ein großer Besuchererfolg. Unter den Besuchern waren viele türkischstämmige Berliner und Berlinbesucher.

<div class="box">

### Das Projekt

Die Ausstellung _Das alte Istanbul in 3D_ ist ein Gemeinschaftsprojekt zwischen dem Städtepartnerschaftsverein Kadıköy e.V und dem Bezirksmuseum Friedrichshain-Kreuzberg in Kooperation mit dem Bezirksamt Kadiköy und den Kulturprojekten Berlin mit Unterstützung der Stiftung Deutsche Klassenlotterie Berlin und mit Unterstützung des Deutschen Historischen Museums.

Projektleitung: Christiane Zieger  
Idee und Konzept: Martin Düspohl  
Recherchen und Texte: Heike Hartmann  
Recherchen, Übersetzungen, Organisation: Özcan Ayanoğlu  
Übersetzungen: Tarik Seden  
Grafische Gestaltung: Ludger Jansen  
Technische Realisierung: Heinz Jansen  
Koordination in Kadıköy : Melike Öztürk, Kadriye Sağlam  
Mitarbeit: Tammo Bork, Bärbel Krenz, Mehmet Başalan  
Beratung: Prof. Erhard Senf, Karsten Hälbig

Danksagung: Wir bedanken uns für die großzügige Unterstützung des Deutschen Historischen Museums Berlin (Herr Dr. Vorsteher, Frau Klein und Frau Krause) sowie für das Entgegenkommen der Bildrechteinhaber: Staatsbibliothek Preußischer Kulturbesitz, ullstein bild, SLUB Fotothek Dresden. Ein besonderer Dank gilt den Leihgebern Karsten Hälbig

</div>