---
title: Kita-Tandem Projekt
date: 2018-10-07 17:36:43 +0000
excerpt: Kita-Tandem Austausch zwischen Kadıköy und Friedrichshain-Kreuzberg
position: 80
thumbnail: "/upload/Meldung_Kita_Tandem.jpg"

---
Wie werden Kinder in Friedrichshain-Kreuzberg und Kadıköy groß, welche Aufgaben erfüllen die Kindertagesstätten? Was verbindet uns bei allen fachlichen und kulturellen Unterschieden? Seit vielen Jahren gibt es ein großes Interesse von Kindertagesstätten aus Friedrichshain-Kreuzbergs Bildungs-und Sozialeinrichtungen in der Partnerstadt Kadıköy kennen zu lernen.

#### 3.bis 10.Juni in Berlin/11. bis 17 November 2018 in Istanbul

Nun gibt es einen TANDEM Austausch mit Erzieherinnen aus Istanbul und Berlin. Beteiligst sind die zwei VAK e.V.  Kitas in der Oranien- und Reichenberger Str. und fünf bezirklichen Kitas aus Kadiköy. Die Kolleginnen aus Kadıköy kamen im Juni nach Berlin, Berliner Kolleginnen werden im November für eine Woche in den Kitas in Kadıköy hospitieren und am Alltag teilnehmen. Der Träger VAK-Kitas e.V. ist seit 30 Jahren in Kreuzberg beheimatet und hat Pionierarbeit in der zweisprachigen Erziehung erbracht.

Neu und anders war für die Erzieherinnen aus Kadıköy waren z.B. die altersgemischten Gruppen, die Selbständigkeit der Kinder und die vielen Ausflüge und Exkursionen außerhalb der Kita. Die „Sternstunden“ für die Kinder, die „Beschwerdekiste“ mit der die Kinder ihre Wünsche oder Ärgernisse zum Ausdruck bringen haben Ihnen gefallen und möchten sie gerne in ihrer Kita umsetzten.

Am Ende des Austausches werden die Ergebnisse gemeinsam besprochen und es wird überlegt werden,wie dieser Austausch fortgesetzt werden kann. Eine Broschüre soll das Projekt dokumentieren und beispielhaft Einblicke in die Welt der Kindererziehung und Kinderbetreuung in Friedrichshain -Kreuzberg und Kadiköy ermöglichen.

_Die Umsetzung des Projektes geschieht mit freundlicher Unterstützung des Regierenden Bürgermeister von Berlin, des Bezirksamts Friedrichshain-Kreuzberg und des Bezirksamts Kadiköy._

![Foto:Özcan Ayanoğlu](/upload/IMG_1667.JPG "Kindergarten in Kadıköy")