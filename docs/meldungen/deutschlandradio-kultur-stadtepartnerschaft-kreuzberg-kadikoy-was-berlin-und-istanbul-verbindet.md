---
thumbnail: "/upload/Meldung_DF_breit.jpg"
title: Deutschlandradio Kultur über Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet
date: 2018-08-13 21:38:14 +0200
excerpt: Der Berliner Bezirk Kreuzberg gilt traditionell als links. Auch im Istanbuler Bezirk Kadiköy fanden in der Geschichte der Stadt große linke Demonstrationen statt. Beide verbindet eine Städtepartnerschaft und ein entsprechend reger Austausch.
position: 6
---
Deutschlandradio Kultur veröffentlichte am 12.09.2016 einen Beitrag mit dem Titel "Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet"

Den Beitrag können Sie auf der [Webseite von Deutschlandradio Kultur](http://www.deutschlandradiokultur.de/staedtepartnerschaft-kreuzberg-kadikoey-was-berlin-und.976.de.html?dram:article_id=365694 "Deutschlandradio Kultur: Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet") lesen oder als [Audio-Beitrag (Podcast)](http://podcast-mp3.dradio.de/podcast/2016/09/12/kreuzberg_kadkoey_verbindungen_in_zeiten_der_krise_drk_20160912_1917_34c9ee0e.mp3 "Deutschlandradio Kultur Podcast: Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet") hören.