---
title: 20jähriges Jubiläum - Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg
  mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy
date: 2018-08-12 08:38:29 +0000
excerpt: In diesem Jahr feiern die Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg
  mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy ihr 20jähriges
  Jubiläum.
position: 1
thumbnail: "/upload/Meldung_Jubiläum.jpg"

---
In diesem Jahr feiern die Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy ihr 20jähriges Jubiläum am 10. September 2016 ab 18.30 Uhr im Nachbarschaftshaus Urbanstraße e.V., Urbanstraße 21 in 10961 Berlin

Dies ist für das Bezirksamt und die BVV von Friedrichshain-Kreuzberg, die Städtepartner Stettin e.V. und Städtepartnerschaftsverein Kadiköy e.V. ein schöner und wichtiger Anlass für eine gemeinsame Feier mit unseren Gästen aus Szczecin und Kadiköy.

In diesen 20 Jahren kam es zu zahlreichen Begegnungen und Projekten zwischen den Partnerstädten auf der offiziellen- und vor allem zivilgesellschaftlichen Ebene. Diese Begegnungen haben für die Beteiligten zu einem besseren Verstehen und zu einem Austausch über die unterschiedlichen und gemeinsamen kulturellen Lebensweisen und Lebenserfahrungen geführt. Wir wünschen uns für die weitere Zukunft eine Fortsetzung dieser lebendigen Begegnungen.

Dies möchten wir am 10. September 2016 ab 18.30 Uhr im Nachbarschaftshaus Urbanstraße e.V., Urbanstraße 21 in 10961 Berlin, mit allen Freunden, Unterstützern und Teilnehmer*innen an städtepartnerschaftlichen Projekten feiern.

Bitte merken Sie sich diesen Termin vor.

Monika Herrmann  
Bezirksbürgermeisterin Friedrichshain -Kreuzberg  
Kristine Jaath  
Vorsteherin der BVV Friedrichshain -Kreuzberg  
Ewa Maria Slaska  
Städtepartner Stettin e.V.  
Christiane Zieger-Ayanoğlu  
Städtepartnerschaftsverein Kadiköy e.V.

[Offizielle Pressemitteilung/Einladung (PDF-Datei)](http://kadikoey-berlin.de/wp-content/uploads/2018/05/20jahre_jubilaeum_save_the_date_.pdf)