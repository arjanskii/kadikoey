---
title: Fotografie-Tandem
date: 2018-09-07 14:14:44 +0000
excerpt: 'Berlin-Istanbul: Tandem mit Fotografen und Fotografinnen aus Berlin und
  Kadiköy'
position: 7
thumbnail: "/upload/Meldung_Fotografie_Tandem.jpg"

---
Vom 23.-29. September wird eine Gruppe junger Fotografen aus Kadiköy nach Berlin kommen. Die Gruppe wird geleitet vom Levent Karaoglu, dem Dozenten für Fotografie am Nazim Hikmet Zentrum in Kadıköy. Die Gruppe wird mit einer Gruppe von Teilnehmern des Photocentrums der vhs unter der Leitung von Klaus W. Eisenlohr Tandem-Gruppen bilden. Sie werden in Kleingruppen an individuellen Fotoprojekten arbeiten, ihren Interessen folgend und in gemeinsamer Gruppenverantwortung.

Die Themen werden sich um Alltag in Berlin, Vergangenheit, Gegenwart, Architektur und Migration drehen.

Das Ziel des Projektes ist, künstlerische Fähigkeiten durch ein internationales Projekt zu erweitern, und interkulturelle Gemeinschaftsarbeit in einem zielorientiertem künstlerischen Projekt zu erfahren. Geplant sind fortführend gemeinsame Ausstellungen der individuellen Arbeiten in Berlin und Istanbul. 

Partnerorganisation ist der Städtepartnerschaftsverein Kadiköy e.V. 

**Ort/Zeit:**
So, 23.09.2018 bis Fr. 28.09.2018.10:00 – 17:00 (mit einem freien Tag)

Foto: Levent Karaoğlu