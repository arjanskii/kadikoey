# Meldungen




## Kita-Tandem Austausch zwischen Kadıköy und Friedrichshain-Kreuzberg

![Foto:Özcan Ayanoğlu](/upload/Meldung_Kita_Tandem.jpg "Kindergarten in Kadıköy")

#### Wie werden Kinder in Friedrichshain-Kreuzberg und Kadıköy groß, welche Aufgaben erfüllen die Kindertagesstätten? Was verbindet uns bei allen fachlichen und kulturellen Unterschieden? Seit vielen Jahren gibt es ein großes Interesse von Kindertagesstätten aus Friedrichshain-Kreuzbergs Bildungs-und Sozialeinrichtungen in der Partnerstadt Kadıköy kennen zu lernen.

**3.bis 10.Juni in Berlin/11. bis 17 November 2018 in Istanbul**

Nun gibt es einen TANDEM Austausch mit Erzieherinnen aus Istanbul und Berlin. Beteiligst sind die zwei VAK e.V.  Kitas in der Oranien- und Reichenberger Str. und fünf bezirklichen Kitas aus Kadiköy. Die Kolleginnen aus Kadıköy kamen im Juni nach Berlin, Berliner Kolleginnen werden im November für eine Woche in den Kitas in Kadıköy hospitieren und am Alltag teilnehmen. Der Träger VAK-Kitas e.V. ist seit 30 Jahren in Kreuzberg beheimatet und hat Pionierarbeit in der zweisprachigen Erziehung erbracht.

Neu und anders war für die Erzieherinnen aus Kadıköy waren z.B. die altersgemischten Gruppen, die Selbständigkeit der Kinder und die vielen Ausflüge und Exkursionen außerhalb der Kita. Die „Sternstunden“ für die Kinder, die „Beschwerdekiste“ mit der die Kinder ihre Wünsche oder Ärgernisse zum Ausdruck bringen haben Ihnen gefallen und möchten sie gerne in ihrer Kita umsetzten.

Am Ende des Austausches werden die Ergebnisse gemeinsam besprochen und es wird überlegt werden,wie dieser Austausch fortgesetzt werden kann. Eine Broschüre soll das Projekt dokumentieren und beispielhaft Einblicke in die Welt der Kindererziehung und Kinderbetreuung in Friedrichshain -Kreuzberg und Kadiköy ermöglichen.

_Die Umsetzung des Projektes geschieht mit freundlicher Unterstützung des Regierenden Bürgermeister von Berlin, des Bezirksamts Friedrichshain-Kreuzberg und des Bezirksamts Kadiköy._

![Foto:Özcan Ayanoğlu](/upload/IMG_1667.JPG "Kindergarten in Kadıköy")



## "Ausstellung: 50 Jahre Kariakatürkei"

![](/upload/KarikaAfis03.jpg)

#### Kita-Tandem Austausch zwischen Kadıköy und Friedrichshain-Kreuzberg

Ausstellung, Lecture, Performance, Stand Up Talks, Workshops  
8.September - 4.November 2018  
Kunstraum Kreuzberg/Bethanien

Weitere Informationen und Programm [finden Sie hier](http://www.kunstraumkreuzberg.de/start.html).




## 'Berlin-Istanbul: Tandem mit Fotografen und Fotografinnen aus Berlin und Kadiköy'

![](/upload/Meldung_Fotografie_Tandem.jpg)

#### In diesem Jahr feiern die Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy ihr 20jähriges Jubiläum am 10. September 2016 ab 18.30 Uhr im Nachbarschaftshaus Urbanstraße e.V., Urbanstraße 21 in 10961 Berlin

Dies ist für das Bezirksamt und die BVV von Friedrichshain-Kreuzberg, die Städtepartner Stettin e.V. und Städtepartnerschaftsverein Kadiköy e.V. ein schöner und wichtiger Anlass für eine gemeinsame Feier mit unseren Gästen aus Szczecin und Kadiköy.

In diesen 20 Jahren kam es zu zahlreichen Begegnungen und Projekten zwischen den Partnerstädten auf der offiziellen- und vor allem zivilgesellschaftlichen Ebene. Diese Begegnungen haben für die Beteiligten zu einem besseren Verstehen und zu einem Austausch über die unterschiedlichen und gemeinsamen kulturellen Lebensweisen und Lebenserfahrungen geführt. Wir wünschen uns für die weitere Zukunft eine Fortsetzung dieser lebendigen Begegnungen.

Dies möchten wir am 10. September 2016 ab 18.30 Uhr im Nachbarschaftshaus Urbanstraße e.V., Urbanstraße 21 in 10961 Berlin, mit allen Freunden, Unterstützern und Teilnehmer*innen an städtepartnerschaftlichen Projekten feiern.

Bitte merken Sie sich diesen Termin vor.

Monika Herrmann  
Bezirksbürgermeisterin Friedrichshain -Kreuzberg  
Kristine Jaath  
Vorsteherin der BVV Friedrichshain -Kreuzberg  
Ewa Maria Slaska  
Städtepartner Stettin e.V.  
Christiane Zieger-Ayanoğlu  
Städtepartnerschaftsverein Kadiköy e.V.

[Offizielle Pressemitteilung/Einladung (PDF-Datei)](http://kadikoey-berlin.de/wp-content/uploads/2018/05/20jahre_jubilaeum_save_the_date_.pdf)



## taz-Artikel über die Städtepartnerschaft

![](/upload/Meldung_taz.jpeg)

#### Die Berliner Tageszeitung taz hat am 07. Januar ausführlich über die Situation in der Türkei und die Rolle der Städtepartnerschaften berichtet.
Der [Artikel "Unter Druck" von Elisabeth Kimmerle](http://www.taz.de/!5368701/ "taz Artikel Unter Druck von Elisabeth Kimmerle") mit der Einleitung "Terror und Repression in der Türkei – das hat auch Auswirkungen auf die Städtepartnerschaft zwischen Berlin und Istanbul. Es kriselt. Und man rückt etwas enger zusammen" wurde am 07.01.2017 im taz.am Wochende veröffentlicht und ist im Internet abrufbar.

[Artikel als PDF-Datei](https://app.forestry.io/sites/lxsfifezxvi0ya/body-media//upload/Artikel_taz.pdf "Artikel als PDF-Datei")



## Deutschlandradio Kultur über Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet

![](/upload/Meldung_DF_breit.jpg)

#### Der Berliner Bezirk Kreuzberg gilt traditionell als links. Auch im Istanbuler Bezirk Kadiköy fanden in der Geschichte der Stadt große linke Demonstrationen statt. Beide verbindet eine Städtepartnerschaft und ein entsprechend reger Austausch.

Deutschlandradio Kultur veröffentlichte am 12.09.2016 einen Beitrag mit dem Titel "Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet"

Den Beitrag können Sie auf der [Webseite von Deutschlandradio Kultur](http://www.deutschlandradiokultur.de/staedtepartnerschaft-kreuzberg-kadikoey-was-berlin-und.976.de.html?dram:article_id=365694 "Deutschlandradio Kultur: Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet") lesen oder als [Audio-Beitrag (Podcast)](http://podcast-mp3.dradio.de/podcast/2016/09/12/kreuzberg_kadkoey_verbindungen_in_zeiten_der_krise_drk_20160912_1917_34c9ee0e.mp3 "Deutschlandradio Kultur Podcast: Städtepartnerschaft Kreuzberg-Kadıköy. Was Berlin und Istanbul verbindet") hören.



## 20jähriges Jubiläum - Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy

![](/upload/Meldung_Jubiläum.jpg)

#### In diesem Jahr feiern die Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy ihr 20jähriges Jubiläum.

In diesem Jahr feiern die Städtepartnerschaften des Bezirks Friedrichshain-Kreuzberg mit der polnischen Stadt Szczecin und dem Istanbuler Bezirk Kadiköy ihr 20jähriges Jubiläum am 10. September 2016 ab 18.30 Uhr im Nachbarschaftshaus Urbanstraße e.V., Urbanstraße 21 in 10961 Berlin

Dies ist für das Bezirksamt und die BVV von Friedrichshain-Kreuzberg, die Städtepartner Stettin e.V. und Städtepartnerschaftsverein Kadiköy e.V. ein schöner und wichtiger Anlass für eine gemeinsame Feier mit unseren Gästen aus Szczecin und Kadiköy.

In diesen 20 Jahren kam es zu zahlreichen Begegnungen und Projekten zwischen den Partnerstädten auf der offiziellen- und vor allem zivilgesellschaftlichen Ebene. Diese Begegnungen haben für die Beteiligten zu einem besseren Verstehen und zu einem Austausch über die unterschiedlichen und gemeinsamen kulturellen Lebensweisen und Lebenserfahrungen geführt. Wir wünschen uns für die weitere Zukunft eine Fortsetzung dieser lebendigen Begegnungen.

Dies möchten wir am 10. September 2016 ab 18.30 Uhr im Nachbarschaftshaus Urbanstraße e.V., Urbanstraße 21 in 10961 Berlin, mit allen Freunden, Unterstützern und Teilnehmer*innen an städtepartnerschaftlichen Projekten feiern.

Bitte merken Sie sich diesen Termin vor.

**Monika Herrmann**  
Bezirksbürgermeisterin Friedrichshain -Kreuzberg  
**Kristine Jaath**  
Vorsteherin der BVV Friedrichshain -Kreuzberg  
**Ewa Maria Slaska**  
Städtepartner Stettin e.V.  
**Christiane Zieger-Ayanoğlu**  
Städtepartnerschaftsverein Kadiköy e.V.

[Offizielle Pressemitteilung/Einladung (PDF-Datei)](/upload/20jahre_jubilaeum_save_the_date_.pdf)
