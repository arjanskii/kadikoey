---
title: Kadıköy
subtitle: 451.453 Einwohner (2017)
date: 2018-08-13 12:45:58 +0000
excerpt: Unser Partnerbezirk Kadıköy liegt auf dem asiatischen Teil von Istanbul,
  an den Ufern des Bosporus und Marmarameer gelegen mit Blick auf die historische
  Halbinsel mit dem Topkapı Palast, der Blauen Moschee und der Haghia Sofia.
thumbnail: "/upload/Header_Kadikoey.JPG"

---

# Kadıköy

<img src="/upload/Header_Kadikoey.jpg" alt="Topkapı Palast, Kadıköy"/>

#### Unser Partnerbezirk Kadıköy liegt auf dem asiatischen Teil von Istanbul,an den Ufern des Bosporus und Marmarameer gelegen mit Blick auf die historische Halbinsel mit dem Topkapı Palast, der Blauen Moschee und der Haghia Sofia.

Mit dem Schiff erreicht man Kadıköy mit dem Bosporus Dampfer (Şehir hatları vapuru) trifft man zuerst auf den historischen Teil des Bezirks. Am linken Ufer liegt der mächtige _Haydarpaşa Bahnhof_, das Wahrzeichen des Bezirks. In die Uferstraße (Rıhtım Caddesi ) münden die steil abfallenden schmalen Straßen des Hafenviertels (Rasimpaşa). Auf der rechten Seite beginnt der _Çarşı (Markt)_ mit seinem lebendigen Treiben. In mitten der vielen Marktgeschäfte befinden sich Kirchen und Moscheen, daneben Kaffees, Restaurants und viele Antiquitätengeschäfte.

<KarteKadikoey  />

### Früher....heute

Lange Zeit war Kadiköy eine recht überschaubare Ansiedlung und galt fast schon als Provinz, obwohl älter als die sogenannte „byzantinische Altsstadt“. Seine Anfänge als Kolonie Kalchedon, später Chalkedon, gehen auf 675 v.Chr. zurück. Heute ist Kadiköy ein moderner pulsierender Stadtbezirk mit knapp einer halben Millionen Einwohner.

### Stadtbild

Das Stadtbild von Kadıköy ist geprägt durch die Lage am Wasser, den Haydarpaşa Bahnhof, das Fenerbahçe-Fußballstadium, den Fenerbahçe Leuchtturm, die Kalamış Marina und bekannte Stadteile wie Yeldeğirmeni, Moda, Caddebostan, Erenköy oder Bostancı.

Die Bagdadstraße (Bagdad Caddesi) ist der Boulevard des Bezirks mit einem lebendigen Mix aus Geschäften, Cafés und Restaurants. Die durch Kadiköy führende E 5 dient als Ausfallstraße und stellt zugleich eine soziale Trennungslinie dar. An ihren Rändern siedelten sich die ehemaligen Gecekondus des Bezirks, nun ragen hier die neuen Hochhäuser von Fikirtepe empor, man spricht halb sarkastisch schon vom Manhattan Kadıköy ´s.  Die Uferstraße (Sahil Yolu) führt am Marmara Meer entlang in die nächsten Bezirke von Istanbul.  Die gleiche Strecke ist auch zu Fuß oder mit dem Fahrrad zu bewältigen. Ein langer Parkstreifen zieht sich an der Küste des Marmarameers entlang und bietet Raum für Sport, Erholung und Entspannung. Mit drei weiteren größere Parkanlagen, dem Özgürlük Park, dem Göztepe Park dem Fenerbahçe Park und zahlreichen kleinen Parkanlagen und Spielplätzen steht der Bezirk für Istanbuler Verhältnisse relativ gut da.

### Verkehr

Das enorme Verkehrsaufkommen und die nicht enden wollenden baulichen Maßnahmen sind eine große Herausforderung für die Stadt. Autos, Busse und Baulaster dominieren den Verkehr, Fußgänger und Fahrradfahrer müssen sich durchkämpfen. Der Verkehr sowie die unzähligen Bauprojekte belasten Bewohner wie Umwelt.

Der an der Uferstraße - Rıhtım Caddesi-befindliche Busbahnhof verteilt jeden Tag hunderttausende von Menschen in die weiteren Bezirke auf der asiatischen Seite. Eine Entlastung soll das Bahn-Verkehrsprojekt „Marmaray“ als auch der Autotunnel „Avrasya“, bringen. Beide führen unter dem Bosporus, haben Stationen in Kadıköy. Sie werden zurzeit gebaut, die Marmaray  hat schon eine Station in Kadiköy. Der Haydarpaşa Bahnhof, einst Ausgangspunkt der Bagdad Bahn wird Ausgangs- und Endstation der Schnellzugverbindungen zwischen Istanbul und Ankara werden.

### Bauen,bauen,bauen..

Kadıköy wird neu gebaut. Nachdem Kadıköy in den 70\`er Jahren beginnend, zuerst zwanzig Jahre in die Fläche und in die Höhe wuchs, führen die neuen Regelungen zum erdbebensicheren Bauen (Kentsel Dönüşüm) zu einem weiteren massiven Umbau der Stadt. Zuerst wird abgerissen um dann noch höher und dichter bauen zu können. Die Zuständigkeit für große Baumaßnahmen liegt beim Ministerium in Ankara. Der Bezirk darf die Bauvorhaben kontrollieren und sich mit den voraussehbaren aber nicht eingeplanten Folgen beschäftigen:  Verkehr, Infrastruktur, Grünflächenbedarf etc.

### Ein lebenswerter Bezirk

Trotzdem gilt Kadıköy immer noch als ein Bezirk, in dem man gut (aber auch teuer) mit hoher Lebensqualität leben kann. Er wird trotz sehr guter Innenstadtlage „ ruhiger“ empfunden als die pulsierenden Innenstadtbezirke  Beşiktas und Beyoğlu auf der europäischen Seite.

Im Ranking um die höchste Lebensqualität für die Bevölkerung steht Kadıköy an vorderster Stelle. Die Versorgung mit Bildungs-, Gesundheits-, Kultur-und Freizeiteinrichtungen, ist außergewöhnlich. Fahrradfahren, früher in Istanbul ein Unikum, ist in Kadıköy immer häufiger anzutreffen und dies nicht nur auf den weitläufigen Fahrradwegen am Marmarameer entlang.

### Demokratisch und Atatürk verehrend

Politisch zeigt sich Kadıköy stabil. Seit Jahren wählen die Bürger von Kadiköy mehrheitlich die republikanische Partei (CHP) des Republikgründers Kemal Atatürk.  Die sozialdemokratisch orientierter CHP und das Bezirksamt Kadıköy befinden sich in den letzten Jahren immer mehr in Opposition zur Regierungspartei AKP.

Das Bezirksamt Kadıköy liegt im Ortsteil Hasanpaşa. Von 1995 bis 2014 war Selami Öztürk (CHP) Bürgermeister. Er unterschrieb 1995 die Städtepartnerschaft mit Kreuzberg. Sein Nachfolger ist Aykurt Nuhoğlu (CHP). In der Türkei wird der Bürgermeister direkt gewählt. Er ernennt seine Stellvertreter/Stellvertreterinnen, die mit ihm die politische Spitze des Bezirksamtes stellen.

Die bezirklichen Projekte zur Frauen-, Jugend-, Behinderten- und Kulturarbeit sollen  die besondere„ Farbe“ des Bezirks demonstrieren.

### Bevölkerungszahl

Nach der Bezirksreform von 2007 und der Abgabe verschiedener Ortsteile an Ataşehir und Ümraniye beläuft sich heute die Einwohnerzahl auf 451.453 Einwohner (2017). Davon sind 203.576 weiblich und 203.576 männlich. 2007 war die Einwohnerzahl noch 744.670 Einwohner.

<div class="box">

<img style="width:200px; display: block; margin-left: auto; margin-right: auto" src="/upload/wappen_kadikoey.gif">

### Kadıköy in Zahlen
* Einwohner  451.453 (2017)
* 1882 lebten in Kadiköy  7.003 Personen.
* Alphabetisierungsrate: 95 %
* 41% der Bevölkerung ist in Istanbul geboren.
* Es gibt 100 private Kindergärten und 5 bezirkliche- und 4 staatliche Kindergärten.
* Von den 54 Grundschulen sind 17 privat.
* Es gibt 25 staatliche, 30 private Gymnasien und 4 Universitäten.
* Das einzige Istanbuler Mädchengymnasium, Erenköy Kiz Lisesi, liegt in Kadiköy.Das Französische Gymansium "Saint Josef Lisesi" befindet sich in Moda.
* Es gibt zwei Frauenhäuser und seit 2005 einen Fahrdienst für Behinderte.
* Das kulturelle Angebot nimmt stetig zu. Es gibt 6 Volksbibliotheken, eine öffentliche Kinderbibliothek, 23 Galerien, 18 Kinos, 9 Theater und Kulturzentren.
* Im Bezirk gibt es über 100 Sportvereine, darunter viele Segelclubs. Der Club "Fenerbahce", eine des ältesten Sporclubs der Türkei, ist aus Kadiköy. Das in Kadiköy befindliche Fußballstadion von Fenerbahce umfaßt ca. 50.000 Zuschauer.

</div>
