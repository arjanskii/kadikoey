---
title: Städtepartnerschaft zwischen Friedrichshain-Kreuzberg und Kadıköy
date: 2018-08-12 08:38:29 +0000
excerpt: Seit 1996 besteht zwischen dem Berliner Bezirk Kreuzberg, später Friedrichshain-Kreuzberg und dem Istanbuler Bezirk Kadıköy eine Städtepartnerschaft.
thumbnail: /upload/Header_Staedtepartnerschaft.jpg

---
# Städtepartnerschaft zwischen Friedrichshain-Kreuzberg und Kadıköy

<img src="/upload/Header_Staedtepartnerschaft.jpg" alt=""/>

Motivation für diese Partnerschaft war der große Anteil von aus der Türkei stammenden Einwohnern Kreuzbergs. Zugleich strebte der Bezirk eine Partnerschaft auf Augenhöhe an und fand in dem modernen und weltoffenen Kadiköy einen idealen Partner. Ziel der Partnerschaft ist es auf „_kulturellem, sozialen und kommunalpolitischen Gebiet eine freundschaftliche Brücke zwischen den Einwohnern und Institutionen beider Gemeinden zu bauen und damit einen wertvollen Beitrag zur Erhaltung und Vertiefung der Völkerverständigung zu leisten_.“

### Städtepartnerschaftsverein Kadıköy

Der Städtepartnerschaftsverein Kadıköy e.V. wurde 1998 gegründet um die partnerschaftlichen Beziehungen durch Kontakte, Begegnungen und Informationen zwischen den Bürger*innen beider Bezirke zu stärken und zu verbreiten. Der Verein nimmt für den Bezirk die Pflege der Partnerschaft wahr. Dafür erhalten wir Fördermittel vom Bezirksamt Friedrichshain-Kreuzberg

### Der Verein

* Fördert den kulturellen AustauschUnterstützt Schulen und   Schulklassen Kontakte zu knüpfen
* Fördert den Erfahrungsaustausch zwischen Fachleuten
* Unterstütz die Entstehung von Partnerschaftsprojektenzwischen Einrichtungen und InstitutionenLeistet Informations- und Öffentlichkeitsarbeit durch Veranstaltungen und Ausstellungen
* Organisiert Bildungs- und Begegnungsreisen

### Aktuell

* Tandem Fotoprojekt mit der Volkshochschule Friedrichshain- Kreuzberg und der Fotografie Abteilung des Nazım Hikmet Kulturzentrum. Leitung: Klaus W. Eisenlohr, Levent Karaoğlu
* Kita Tandem Projekt mit den VAK -Kitas in Friedrichshain-Kreuzberg und den bezirklichen Kitas in Kadıköy
* Austausch zwischen den Musikschulen
* Austausch zwischen den Sozialabteilungen beider Bezirksverwaltungen

### Veränderung und Zusammenhalt über Jahre

In den 22 Jahren der Städtepartnerschaft hat sich viel verändert. Kadıköy ist durch Bezirksabgabe kleiner geworden, Kreuzberg hat sich mit Friedrichshain verbunden. 1996 waren in der Kadiköy Bezirksparlament vier Parteien (CHP, DYP, ANAP, RP) heute sind es zwei Parteien (CHP, AKP).1996 gab es in der Kreuzberger BVV 3 Parteien (Grüne, SPD, CDU) heute sind es 8 Parteien. Während wir 1996 noch mit gewisser Hoffnung auf einen Eintritt der Türkei in die EU geschaut haben, erscheint er heute weit entfernt.

Für die Städtepartner ist jedoch der Wunsch nach Zusammenarbeit in kommunalen Themen gewachsen und stärker denn je.
