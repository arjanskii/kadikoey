module.exports = {
  title: '',
  description: 'Willkomen beim Städtepartnerschaftsverein Kadıköy e.V.',
  locales: {
    '/': {
      lang: 'de-DE',
      title: '',
      description: 'Willkomen beim Städtepartnerschaftsverein Kadıköy e.V.',
    },
    '/tr/': {
      lang: 'tr-TR',
      title: '',
      description: 'Lorem ipsum',
    },
  },
  themeConfig: {
    docsDir: 'docs',
    sidebarDepth: 2,
    logo: "/upload/Logo.png",
    footer: "Ⓒ 2018 Städtepartnerschaftsverein Kadıköy e.V. | c/o Zieger-Ayanoğlu, Yorckstr.84A | D-10965 Berlin",
    locales: {
      '/': {
        label: 'Deutsch',
        selectText: 'Sprache',
        sidebar: {
          '/meldungen/': [
              '/meldungen/'
          ],
          '/ueber/': [
              '/ueber/'
          ]
        },
        nav: [
          {text: '', link: '/'},
          {text: 'Städtepartnerschaft',
            items: [
              {text: 'Projekt', items: [
                  {text: 'Die Städtepartnerschaft', link: '/staedtepartnerschaft/'}
                ]},
              {text: 'Stadtteile', items: [
                  {text: 'Kadikoey (Istanbul)', link: '/stadtteile/kadikoey'},
                  {text: 'Friedrichshain-Kreuzberg (Berlin)', link: '/stadtteile/friedrichshain-kreuzberg'}
                ]}
            ]
          },
          // {text: 'Städtepartnerschaft',
          //   items: [
          //     {text: 'Das alte Istanbul in 3D', link: '/projekte/das-alte-istanbul-in-3d'},
          //     {text: 'Begegnungs- und Bildungsreisen', link: '/projekte/bildungsreisen'},
          //     {text: 'Musik', link: '/projekte/musik'},
          //     {text: 'Fotografie', link: '/projekte/fotografie'},
          //     {text: 'Kindertagesstätten', link: '/projekte/kitas'}
          //   ]
          // },
          {text: 'Projekte',
            items: [
              {text: 'Das alte Istanbul in 3D', link: '/projekte/das-alte-istanbul-in-3d'},
              {text: 'Begegnungs- und Bildungsreisen', link: '/projekte/bildungsreisen'},
              {text: 'Musik', link: '/projekte/musik'},
              {text: 'Fotografie', link: '/projekte/fotografie'},
              {text: 'Kindertagesstätten', link: '/projekte/kitas'}
            ]
          },
          {text: 'Meldungen', link: '/meldungen/'},
          {text: 'Über Uns', link: '/ueber/'},
          // {text: 'Über Uns',
          //     items: [
          //       {text: 'Verein', link: '/ueber/ueberuns'},
          //       {text: 'Vorstand', link: '/ueber/vorstand'},
          //       {text: 'Chronik', link: '/ueber/chronik'},
          //       {text: 'Satzung', link: '/ueber/satzung'},
          //       {text: 'Mitarbeit', link: '/ueber/mitarbeit'},
          //       {text: 'Spenden', link: '/ueber/spenden'},
          //       { text: 'Kontakt', link: 'mailto:kadikoey.ev@web.de', external: true }
          //     ]
          // }
        ],
      },
      '/tr/': {
        label: 'Türkçe',
        selectText: 'Dil',
        sidebar: {
          '/tr/meldungen/': [
            '/tr/meldungen/'
          ],
          '/tr/ueber/': [
            '/tr/ueber/'
          ]
        },
        nav: [
          {text: '', link: '/'},
          {text: 'Kardeş Kent İlişkilerini',
            items: [
              {text: 'Projekt', items: [
                  {text: 'Die Städtepartnerschaft', link: '/tr/staedtepartnerschaft/'}
                ]},
              {text: 'Stadtteile', items: [
                  {text: 'Kadikoey (Istanbul)', link: '/tr/stadtteile/kadikoey'},
                  {text: 'Friedrichshain-Kreuzberg (Berlin)', link: '/tr/stadtteile/friedrichshain-kreuzberg'}
                ]}
            ]
          },
          // {text: 'Städtepartnerschaft',
          //   items: [
          //     {text: 'Das alte Istanbul in 3D', link: '/projekte/das-alte-istanbul-in-3d'},
          //     {text: 'Begegnungs- und Bildungsreisen', link: '/projekte/bildungsreisen'},
          //     {text: 'Musik', link: '/projekte/musik'},
          //     {text: 'Fotografie', link: '/projekte/fotografie'},
          //     {text: 'Kindertagesstätten', link: '/projekte/kitas'}
          //   ]
          // },
          {text: 'Projeler',
            items: [
              {text: 'Das alte Istanbul in 3D', link: '/tr/projekte/das-alte-istanbul-in-3d'},
              {text: 'Begegnungs- und Bildungsreisen', link: '/tr/projekte/bildungsreisen'},
              {text: 'Musik', link: '/tr/projekte/musik'},
              {text: 'Fotografie', link: '/tr/projekte/fotografie'},
              {text: 'Kindertagesstätten', link: '/tr/projekte/kitas'}
            ]
          },
          {text: 'Haberler', link: '/tr/meldungen/'},
          {text: 'Hakkımızda', link: '/tr/ueber/'},
          // {text: 'Über Uns',
          //     items: [
          //       {text: 'Verein', link: '/ueber/ueberuns'},
          //       {text: 'Vorstand', link: '/ueber/vorstand'},
          //       {text: 'Chronik', link: '/ueber/chronik'},
          //       {text: 'Satzung', link: '/ueber/satzung'},
          //       {text: 'Mitarbeit', link: '/ueber/mitarbeit'},
          //       {text: 'Spenden', link: '/ueber/spenden'},
          //       { text: 'Kontakt', link: 'mailto:kadikoey.ev@web.de', external: true }
          //     ]
          // }
        ],
      },
    },
  },
};
